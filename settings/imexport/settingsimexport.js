var dataSetId = {
    settings: 0,
    subscriptions: 1,
    history: 2,
    playlists: 3
},
dataControl = new OptionsMenuSelectable(i18n('data-selection'),'checkbox');

var settingsDataIEaction;
function settingsDataIE(action) {
    switch(action) {
        case 0: settingsDataIEaction = (ti)=>{
			importFile()
			.then((f)=>{
				settingsImport(f, ti);
			})
			.catch(settingsDataIEcancel);
        };
        break;
        case 1: settingsDataIEaction = settingsExport; break;
        default: return;
    }

    //get the selection...
    curpage = 1;
    updatenavbar();
    dataControl.menuViewToggle(true,1,0);
}
function settingsDataIEdone() {
    dataControl.menuViewToggle(false);
    imexMenu.navigate(0);
    settingsDataIEaction = undefined;
}
function settingsDataIEcancel() {
    progressDisplayVisToggle(false);
    curpage = 0;
    updatenavbar();
    settingsDataIEdone();
}
function dataSelectionK(k) {
    switch(k.key) {
        case 'Enter': //toggle
            dataControl.selectItem(actEl().tabIndex); 
            break;
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            dataControl.navigate(u || 1);
            break;
        case 'SoftRight': //GO!!!
            settingsDataIEaction(dataControl.getValue().value);
            curpage = -100;
            settingsDataIEdone();
            break;
        case 'SoftLeft': //back
        case 'Backspace':
            settingsDataIEcancel();
            break;
            
    }
}

//f = file, toinclude = array of dataSetId
function settingsImport(f, toinclude) {
    file = pJSON(f);

    if(file) {
        settingsImportInProgress = true;
        progressDisplayVisToggle(true);
        progressDisplay(i18n('import-progress-file-successfully-parsed'));

        var mds //mdatastore - for use below.
        ;

        //default settings.
        var 
        //how is the metadata saved? 0 = directly, 1 = separately.
        metadataSaveMethod = 1;

        //version stuff
        var ver = null;
        if('version' in file) {
            var dateMatch = file.version.match(/\d{4}-\d{2}-\d{2} \d{2}:\d{2}/);
            if(0 in dateMatch) {
                ver = (new Date(dateMatch[0]));

                if(ver < (new Date('2022-03-27 18:40'))) {
                    metadataSaveMethod = 0;
                }
            }
        }

        //misc stuff
        var mds = file.mdataStore;

        //actually parsing the file
        var ready = 0,
        checkAllDone = ()=>{
            ready++;
            if(ready === toinclude.length) {
                progressDisplay(i18n('import-progress-okay-all-done'));
                setTimeout(window.parent.fullReload, 3000);
                
            }
        };

        //settings
        if(
            'settings' in file &&
            toinclude.indexOf(dataSetId.settings) !== -1
        ) {
            var importSts = Object.keys(file.settings),
            existingSts = Object.keys(settingsList);
            for(var i = 0; i < importSts.length; i++) {
                var csts = importSts[i];
                if(existingSts.indexOf(csts) !== -1) {
                    if([4,5].indexOf(settingsList[csts].type) === -1) {
                        updateSetting(
                            csts,
                            JSON.stringify(file.settings[csts]),
                            true
                        );
                        if('action' in settingsList[csts]) {
                            settingsList[csts].action();
                        }
                    }
                }
            }
            progressDisplay(i18n('import-progress-settings-updated', {u: importSts.length}));
        }
        checkAllDone(); //calling it now because settings is synchronous

        //history
        var historyProg = 0;
        window.parent.allPageHistory.clear().then(()=>{
            if(
                'history' in file  &&
                toinclude.indexOf(dataSetId.history) !== -1
            ) {
                var th = file.history;
                if(th.length === 0) {
                    histProgDisp.updatePercent(0, 0);
                    checkAllDone();
                } else {
                    var histProgDisp = new ProgressDisplayProgBars(i18n('history') + ' '),
                    pushHistFromFile = ()=>{
                        var cHistItem = th[historyProg];

                        if(metadataSaveMethod === 1) {
                            if(cHistItem.id in mds) {
                                objectMerge(
                                    mds[cHistItem.id],
                                    cHistItem
                                );
                            }
                        }

                        pushToAllHistory(
                            cHistItem.id,
                            cHistItem.type,
                            cHistItem.name || makeUnknownTitleItem(cHistItem.id),
                            cHistItem.exdata || {},
                            cHistItem.time,
                            ()=>{
                                historyProg++;
    
                                histProgDisp.updatePercent(historyProg, th.length);
    
                                if(historyProg === th.length) {
                                    checkAllDone();
                                } else {
                                    //setTimeout(pushHistFromFile, 500);
                                    pushHistFromFile();
                                }
                            }
                        )
                    };
                    pushHistFromFile();
                }
            } else {
                checkAllDone();
            }
        });

        //subscriptions
        channelSubscriptions.clear().then(()=>{
            if(
                'subscriptions' in file &&
                toinclude.indexOf(dataSetId.subscriptions) !== -1
            ) {
                var sbs = file.subscriptions,
                histProgDisp = new ProgressDisplayProgBars(i18n('subscriptions-menu-item') + ' '),
                subscriptionsProcessed = 0;

                if(metadataSaveMethod === 0) {
                    sbs = Object.keys(file.subscriptions);
                }

                if(sbs.length === 0) {
                    histProgDisp.updatePercent(0, 0);
                    checkAllDone();
                } else {
                    for(var i = 0; i < sbs.length; i++) {
                        var subId = sbs[i],
                        subInfo = file.subscriptions[subId];

                        if(metadataSaveMethod === 1) {
                            if(subId in mds) {
                                var tsi = mds[subId];
                                subInfo = {
                                    name: tsi.name,
                                    img: tsi.image
                                };
                            }
                        }

                        subscriptionsAdd(
                            subId,
                            subInfo.name || makeUnknownTitleItem(subId),
                            subInfo.img,
                            true,
                            ()=>{
                                subscriptionsProcessed++;
        
                                histProgDisp.updatePercent(subscriptionsProcessed, sbs.length);
        
                                if(subscriptionsProcessed === sbs.length) {
                                    checkAllDone();
                                }
                            }
                        );
                    }
                }
            } else {
                checkAllDone();
            }
        });

        //playlists
        window.parent.clearPlaylistData(()=>{
            if(
                'playlists' in file &&
                toinclude.indexOf(dataSetId.playlists) !== -1
            ) {
                var pllProgDisp = new ProgressDisplayProgBars(i18n('playlists-menu-item') + ' '),
                totalVideos = 0,
                processedVideos = 0,
                pllLength = file.playlists.length;
                if(pllLength === 0) {
                    pllProgDisp.updatePercent(0, 0);
                    checkAllDone();
                } else {
                    var cpl = -1,
                    workOnPlaylist = ()=>{
                        cpl++;
                        if(pllLength === cpl) {
                            checkAllDone();
                        } else {
                            var cplo = file.playlists[cpl];
                            userPlaylistManage(
                                cplo.metadata.name,
                                0,
                                null,
                                (nPlId)=>{ //nPlId = "new Playlist Id"
                                    var curVidInsert = -1,
                                    pl = cplo.list,
                                    insertVid = ()=>{
                                        curVidInsert++;
                                        if(curVidInsert === pl.length) {
                                            workOnPlaylist();
                                        } else {
                                            pllProgDisp.updatePercent(++processedVideos, totalVideos);

                                            userPlaylistModify(
                                                nPlId,
                                                0,
                                                pl[curVidInsert],
                                                null,
                                                insertVid,
                                                (e)=>{
                                                    console.error('PLAYLIST VIDEO IMPORT ERROR!!',e);
                                                    insertVid();
                                                }
                                            );
                                        }
                                    };

                                    insertVid();
                                },
                                (e)=>{
                                    console.error('PLAYLIST CREATE ERROR!!',e);
                                    workOnPlaylist();
                                }
                            );
                        }
                    };

                    file.playlists.forEach((pl)=>{totalVideos += pl.list.length;});
                    workOnPlaylist();
                }
            } else {
                checkAllDone();
            }
        });

    } else {
        alertMessage(i18n('settings-file-parse-failed'), 5000, 3);
    }
}

function settingsExport(toinclude) {
    progressDisplayVisToggle(true);

    var exp = {
        version: versionNumber,
        settings: {},
        subscriptions: [],
        history: [],
        historyLast: 0,
        playlists: [],
        mdataStore: {}
    },
    ready = 0, //see allDone()

    allDone = ()=>{ //this is called when everything is done.
        ready++;
        console.log(ready);
        if(ready === 4) {
            //need 4 to be ready.
            //things to ready: subs, history, playlist, mdatacache
            var blb = new Blob([JSON.stringify(exp)]),
            blblink = document.createElement('a');
    
            blblink.download = i18n('settings-file-export-name') + (new Date()).getTime() + '.json';
            blblink.href = URL.createObjectURL(blb);
            document.body.appendChild(blblink);
            blblink.click();
            blblink.remove();
            setTimeout(()=>{
               URL.revokeObjectURL(blblink.href);
            },5000);

            settingsDataIEcancel();
        }
    };

    console.log(
        toinclude.indexOf(dataSetId.settings) !== -1,
        toinclude.indexOf(dataSetId.subscriptions) !== -1,
        toinclude.indexOf(dataSetId.history) !== -1,
        toinclude.indexOf(dataSetId.playlists) !== -1
    )

    /* settings */
    if(toinclude.indexOf(dataSetId.settings) !== -1) {
        Object.keys(settingsList).forEach((e)=>{
            if([4,5].indexOf(settingsList[e].type) === -1) {
                exp.settings[e] = getSettingValue(e);
            }
        });
        
    }

    /* subscriptions */
    if(toinclude.indexOf(dataSetId.subscriptions) !== -1) {
        var chnProgDisp = (new ProgressDisplayProgBars(i18n('subscriptions-menu-item') + ' '));
        channelSubscriptions.length().then((length)=>{
            if(length === 0) {
                chnProgDisp.updatePercent(0, 0);
                allDone();
            } else {
                var sp = 0;
                channelSubscriptions.iterate((v,i)=>{
                    exp.subscriptions.push(i);
                    chnProgDisp.updatePercent(sp + 1, length);
                    sp++;
                    if(sp === length) {
                        allDone();
                    }
                });
            }
        });
    } else {
        allDone();
    }
    

    /* history */
    if(toinclude.indexOf(dataSetId.history) !== -1) {
        var aph = window.parent.allPageHistory;
        aph.length().then((length)=>{
            if(length === 0) {
                aphProgDisp.updatePercent(0, 0);
                allDone();
            } else {
                var aphlen = 0,
                aphProgDisp = (new ProgressDisplayProgBars(i18n('history') + ' ')),
                historyProgressCheck = (p)=>{
                    exp.history.push(p);
                    aphProgDisp.updatePercent(exp.history.length, aphlen);
                    if(exp.history.length === aphlen) {
                        allDone();
                    }
                };
    
                aph.getItem('lastItem').then((li)=>{
                    if(li !== null) {length--;}
        
                    aphlen = length;
                    length = undefined;
        
                    aph.iterate((v,histNum)=>{
                        if(histNum !== 'lastItem') {
                            delete v.exdata;
                            if(v.type !== 'search') {
                                delete v.name;
                            }
                            historyProgressCheck(v);
                        }
                        

                    });
                });
            }
        });
    } else {
        allDone();
    }
    

    /* playlists */
    if(toinclude.indexOf(dataSetId.playlists) !== -1) {
        var plMdata = userPlaylistMdataCache(),
        plProgDisp = new ProgressDisplayProgBars(i18n('playlists-menu-item') + ' '),
        availList = Object.keys(plMdata), availListLen = availList.length;
        if(availListLen === 0) {
            plProgDisp.updatePercent(0,0);
            allDone();
        } else {
            availList.forEach((plid, i)=>{
				i = i + 1;
                userPlaylistStore.getItem(plid).then((playlistItems)=>{
                    delete plMdata[plid].count;
                    exp.playlists.push({
                        metadata: plMdata[plid],
                        list: playlistItems
                    });

                    plProgDisp.updatePercent(i, availListLen);

                    if(i === availListLen) {
                        allDone();
                    }
                });
            })
        }
    } else {
        allDone();
    }

    /* mdatacache */
    var mdataNeeded = false;
    for(var i = 0; i < toinclude.length; i++) {
        if(
            [
                dataSetId.history,
                dataSetId.playlists,
                dataSetId.subscriptions
            ].indexOf(toinclude[i]) !== -1
        ) {
            mdataNeeded = true;
            break;
        }
    }

    console.log('mdata', mdataNeeded);

    if(mdataNeeded) {
        var mdataProgDisp = new ProgressDisplayProgBars(i18n('metadata') + ' ');
        mdataStore.length().then((length)=>{
            if(length === 0) {
                mdataProgDisp.updatePercent(0, 0);
            } else {
                mdataStore.iterate((v,k,i)=>{
                    exp.mdataStore[k] = v;
                    mdataProgDisp.updatePercent(i, length);
                }).then(allDone);
            }
        });
    } else {
        allDone();
    }
    
}
