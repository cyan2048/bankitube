var curpage = 0,
imexMenu = new Menu(eid('items'));

window.addEventListener('DOMContentLoaded',()=>{
    //main menu
    [
        'import-application-data',
        'export-application-data',
        'import-subs-youtube',
        'import-subs-newpipe'
    ].forEach((label)=>{
        var labelEl = imexMenu.addOption(i18n(label));
        labelEl.classList.add('text-no-wrap');
    });

    //data selection menu
    [
        'settings',
        'subscriptions-menu-item',
        'history',
        'playlists-menu-item'
    ].forEach((label)=>{
        dataControl.addOption(i18n(label), true);
    });
    

    imexMenu.navigate(0);
    updatenavbar();
    disableControls = false;
    allowBack = false;
});

function overwriteWarning(impAct) {
    messageBox(
        i18n('are-you-sure'),
        i18n('import-overwrite-warning'),
        {
            left: messageBoxOption(
                messageBoxDefaultBackCallback,
                i18n('no')
            ),
            right: messageBoxOption(
                ()=>{
                    messageBoxDefaultBackCallback();
                    impAct();
                },
                i18n('yes')
            )
        }
    );
}

function localupdatenavbar() {
    var nb;
    switch(curpage) {
        case 0: //main
            nb = [
                
            ];
            break;
        case 1:
    }

    outputNavbar(
        [
            [i18n('back'), i18n('select')],
            [i18n('cancel'), i18n('select'), i18n('go')]
        ][curpage]
    );
}

function keyHandler(k) {
    switch(curpage) {
        case 1: //data selection
            dataSelectionK(k); break;
        case 0: //main
            switch(k.key) {
                case 'SoftLeft':
                case 'Backspace':
                    location = '/home/index.html';
                    break;
                case 'ArrowUp':
                    var u = -1;
                case 'ArrowDown':
                    imexMenu.navigate(u || 1);
                    break;
                case 'Enter': 
                    switch(actEl().tabIndex) {
                        case 0: //import all
                            overwriteWarning(()=>{settingsDataIE(0)});
                            break;
                        case 1: //export all
                            settingsDataIE(1);
                            break;
                        case 2: //import yt sub
							overwriteWarning(()=>{
								importFile()
								.then(importSubscriptionsYoutube)
								.catch(settingsDataIEcancel);
							});
                            break;
                        case 3: //import newpipe sub
                            overwriteWarning(()=>{
								importFile()
								.then(importSubscriptionsNewpipe)
								.catch(settingsDataIEcancel);
                            });
                            break;
                    }
                    break;
            }
    }
    
}
