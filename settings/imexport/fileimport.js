function convertToText(blob) {
	return new Promise((resolve, reject)=>{
		let fr = new FileReader();
		fr.readAsText(blob);
		fr.onload = (r)=>{
			resolve(r.target.result);
		};
		fr.onerror = reject;
	});
};

function importFile() {
	return new Promise(function(resolve, reject){
		
		if('MozActivity' in window) {
			//moz activity exists
			let lfa = new MozActivity({
				name: 'pick'
			});

			lfa.onsuccess = (res)=>{
				console.log(res);
				res = res.target.result;

				if(!('blob' in res)) {
					reject('MozActivity resulted in success, however there is no blob available.');
					return;
				}
				
				convertToText(res.blob).then(resolve);
			};

			lfa.onerror = (e)=>{
				console.error(e);
				reject(e);
			}	
		} else {
			let filepicker = document.createElement('input');
			filepicker.type = 'file';
			filepicker.classList.add('hidden');
			document.body.appendChild(filepicker);
			
			filepicker.addEventListener('change', ()=>{
				convertToText(filepicker.files[0]).then(resolve);
			});
			
			document.addEventListener('visibilitychange', function vc(ev){
				if(document.visibilityState === 'visible') {
					document.removeEventListener('visibilitychange', vc);
					filepicker.remove();
					if(filepicker.files.length === 0) {
						reject();
					}
				}
			});
			filepicker.click();
		}
	});
}
