function progressDisplayVisToggle(vis) {
    var p = eid('progress');
    p.innerHTML = '';
    if(vis) {
        p.classList.remove('hidden');
    } else {
        p.classList.add('hidden');
    }

    disableControls = !!vis;
    disableNavKey = !!vis;
}

function progressDisplay(addition) {
    var ad = document.createElement('div');
    if(addition instanceof HTMLElement) {
        ad.appendChild(addition);
    } else {
        ad.textContent = addition;
    }
    eid('progress').appendChild(ad);
    eid('progress').scrollTop = eid('progress').scrollHeight;

    return ad;
}

class ProgressDisplayProgBars {
    constructor(custText) {
        if(custText === undefined) {custText = '';}
        this.cText = custText;

        this.progBar = document.createElement('progress');
        this.progBar.value = 0;

        this.text = progressDisplay(this.makeProgText(0,0));
        this.text.style.textAlign = 'center';

        var pcnt = progressDisplay(this.progBar);
        pcnt.style.textAlign = 'center';
    }

    makeProgText(a,t) {
        return `${this.cText}${Math.floor(this.progBar.value * 100)}% (${a}/${t})`
    }

    updatePercent(a,t) {
		let barVal;
		if(a === t && t === 0) {
			barVal = 1;
		} else {
			barVal = a / t;
		}
		
        this.progBar.value = barVal;
        this.text.textContent = this.makeProgText(a,t);
    }
}