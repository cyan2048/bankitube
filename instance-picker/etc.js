var curpage = 0, lastfocus = 0;

function localupdatenavbar() {
    switch(curpage) {
        case 0:
            outputNavbar(
                i18n('back'),
                i18n('use'),
                i18n('options')
            );
            break;
        case 1:
            outputNavbar(
                '',
                i18n('select'),
                i18n('back')
            );
            break;
    }
}
function keyHandler(k) {
    switch(curpage) {
        case 0:
            switch(k.key) {
                case 'ArrowUp':
                    var u = -1;
                case 'ArrowDown': 
                    navigatelist(
                        actEl().tabIndex, 
                        ecls('instance'), 
                        u || 1
                    ); 
                    k.preventDefault(); 
                    break;
                case 'SoftRight': 
                    lastfocus = actEl().tabIndex;
                    eid('options').classList.remove('hidden');
                    ecls('options')[0].focus();
                    curpage = 1;
                    break;
                case 'Enter': 
                    localStorage.setItem('setting-invidious-instance', actEl().dataset.url);
                    localStorage.removeItem('cache');
                    alertMessage(
                        i18n('instance-picker-instance-changed', {url: actEl().dataset.url}), 
                        5000, 0);
                case 'Backspace': 
                case 'SoftLeft':
                    backHistory(); 
                    break;
            }
            break;
        case 1:
            switch(k.key) {
                case 'ArrowUp':
                    var u = -1;
                case 'ArrowDown': 
                    navigatelist(
                        actEl().tabIndex, 
                        ecls('options'), 
                        u || 1
                    ); 
                    k.preventDefault(); 
                    break;
                case 'Enter': 
                    switch(actEl().dataset.action) {
                        case 'switch-online': 
                            toggleOnlineList(); 
                            lastfocus = 0;
                            break;
                        case 'test-instances': 
                            if(probing.rqs.length === probing.done) {
                                testAll();
                            } else {
                                testAllAbort();
                            }
                            break;
                    }
                case 'Backspace': 
                case 'SoftRight':
                    eid('options').classList.add('hidden');
                    ecls('instance')[lastfocus].focus();
                    curpage = 0;
                    break;
            }        
    }
}
