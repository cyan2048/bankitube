(()=>{
    //scripts
    var cj = '/common/js/';
    [
        cj + 'settings',
        cj + 'localization',
        cj + 'messageBox',
        cj + 'commonElements',
        cj + 'control',
        cj + 'etcf',
        cj + 'cache',
        cj + 'net',
        cj + 'data',
        cj + 'randomVideoList',
        'etc',
        'script',
    ].forEach((fn)=>{
        addGlobalReference(0, fn);
    });

    var cs = '/common/style/';
    //styles
    [
        cs + 'all',
        cs + 'widgets',
        cs + 'theme/default',
        'style'
    ].forEach((fn)=>{
        addGlobalReference(1, fn);
    });
})();