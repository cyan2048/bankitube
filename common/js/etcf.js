var navbar = {
    "myself": document.getElementById('navbar'),
    "left": document.getElementById('navbar-left'),
    "right": document.getElementById('navbar-right'),
    "center": document.getElementById('navbar-center')
},

urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/,
//http://urlregex.com/
youtubeRegex = /^https?:\/\/(?:(?:www|m)\.)?((?:youtube\.com\/(?:watch\?v=|shorts\/|playlist\?list=|(?:channel|embed)\/))|youtu\.be\/)([a-zA-Z0-9_\-]+)$/,
//note: this regex will also match some strange urls like m.youtu.be/whatever or www.youtu.be/whatever but its fine, its good enough.

emptyFn = ()=>{}
;

function outputNavbar(l,c,r) {
    var na = [
        'left',
        'center',
        'right',
    ], 
    oa;

    if(Array.isArray(l)) {
        oa = l;
    } else {
        oa = [l,c,r];
    }

    for(var i = 0; i < 3; i++) {
        var cn = navbar[na[i]];
        if(oa[i] instanceof HTMLElement) {
            cn.innerHTML = '';
            oa[i].classList.add('icon', 'center');
            cn.appendChild(oa[i]);
        } else {
            cn.textContent = oa[i];
        }
    }
}
function createImg(path) {
    var i = document.createElement('img');
    i.src = path;
    return i;
}

if('mozAudioChannelManager' in navigator) {
	navigator.mozAudioChannelManager.volumeControlChannel = 'content';
}

function eid(e){return document.getElementById(e)}
function ecls(e){return document.getElementsByClassName(e)}
function actEl(){return document.activeElement}

var timeformatHoursDisplay = getSettingValue('video-hours-time-display');
function timeformat(input) {
    if(input) {
        var m = Math.floor(input / 60), s = Math.floor(input % 60), h = '';


        if(m > 59 && timeformatHoursDisplay) {
            h = Math.floor(m / 60) + ':';
            m = Math.floor(m % 60);
        } 
        if(m < 10) {
            m = '0' + m;
        }
        if(s < 10) {
            s = '0' + s;
        }
        return h + m + ':' + s;
    } else {
        return '';
    }
}

function unixdatetimeformat(input) {return unixdateformat(input) + ' ' + unixtimeformat(input);}

function unixdateformat(input) { //expecting unix time seconds only; no milis. returns a string with year, month, day, hour, minute, and second
    var dateinit = new Date(input * 1000);
    var times = [
        dateinit.getFullYear(),
        dateinit.getMonth() + 1,
        dateinit.getDate()
    ];
    for(i=0;i<times.length;i++) {
        if(times[i] < 10) {times[i] = '0' + times[i];}
    }
    
    var rs = dateFormatDef;

    ['(y)', '(m)', '(d)'].forEach((v, i)=>{
        rs = rs.replace(v, times[i]);
    });

    return rs;
    //return times[0] + '-' + times[1] + '-' + times[2];
}

function unixtimeformat(input) { //expecting unix time seconds only; no milis. returns a string with year, month, day, hour, minute, and second
    var dateinit = new Date(input * 1000),
    times = [
        dateinit.getHours(),
        dateinit.getMinutes(),
        dateinit.getSeconds()
    ],
    zp = (n)=>{
        if(n < 10) {
            return '0' + n;
        } else {
            return n;
        }
    };

    var rs = timeFormatDef;

    ['(_hh)', '(_h)', '(hh)', '(h)', '(m)', '(s)', '(af)'].forEach((v)=>{
        if(rs.match(v)) {
            var x = '';
            switch(v) {
                case '(_hh)': x = zp(times[0]); break;
                case '(_h)':  x = times[0]; break;
                case '(hh)':  x = zp(times[0] % 12 || 12); break;
                case '(h)': x = (times[0] % 12) || 12; break;
                case '(m)': x = zp(times[1]); break;
                case '(s)': x = zp(times[2]); break;
                case '(af)': x = (times[0] < 12? i18n('time-am') : i18n('time-pm')); break;
            }
            rs = rs.replace(v, x);
        }
    });

    return rs;
}

function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
}

function randomHexString(len) {
    if(
        isNaN(len) ||
        !isFinite(len)
    ) {
        len = 32;
    }
    var s = '';
    for(var i = 0; i < len; i++) {
        s += '0123456789abcdef'.charAt(Math.floor(Math.random() * 16));
    }
    return s;
}
function randomizeArray(ia) {
    var oa = [];
    while(ia.length !== 0) {
        oa.push(ia.splice(Math.floor(Math.random() * ia.length), 1)[0]);
    }
    return oa;
}
function selectRandomFromArray(a) {
    return a[Math.floor(Math.random() * a.length)];
}
function arrayMove(arr, old_index, new_index) { //https://stackoverflow.com/a/5306832
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    //return arr; // for testing
}

function makeThrobber(container) {
    var t = document.createElement('div');
    t.classList.add('throbber');
    if(container) {
        var c = document.createElement('div');
        c.appendChild(t);
        return c;
    }
    return t;
}

function makeUnknownTitleItem(id) {
    return 'ID: ' + id;
}

function vidIsDed(vidObj) {
    //console.log(vidObj);
    return ([
        '[Private video]',
        '[Deleted video]'
    ].indexOf(vidObj.title) !== -1)
    &&
    vidObj.authorId === ''
    &&
    vidObj.lengthSeconds === 0;
}

var linkUtil = {
    formYtLink: (after)=>{return 'https://www.youtube.com' + after},
    formYtVideoLink: (id)=>{return linkUtil.formYtLink('/watch?v=' + id)},
    formYtChannelLink: (id)=>{return linkUtil.formYtLink('/channel/' + id)},
    formYtPlaylistLink: (id)=>{return linkUtil.formYtLink('/playlist?list=' + id)},
    formYtThumbnail: (vidId)=>{return `https://i3.ytimg.com/vi/${vidId}/default.jpg`}
};

var animations = {
    fadeOut: (e)=>{
        animations.generic(e, 'fadeOutAnim');
    },
    
    generic: (e, cn)=>{
        e.classList.remove(cn);
        void(e.offsetHeight);
        e.classList.add(cn);
    }
};

function checkIfScreenHori() {
    return screen.orientation.type.indexOf('portrait') === -1;
}

function focusInput(ip) {
    ip.focus();
    var ln = ip.value.length;
    setTimeout(()=>{
        ip.setSelectionRange(ln,ln); 
    },1);
}

function navigatelist(index,list,move) { //RETURNS the current index of the thing.
    index += move;
    if(index >= list.length) {
        index = 0;
    } else if(index <= -1) {
        index = list.length - 1;
    }

    if(list[index] instanceof HTMLElement) {
        list[index].focus();
    }
    return index;
}

function elBounds(cn, el) {
    return {
        'ct': cn.scrollTop,
        'cb': cn.scrollTop + cn.clientHeight,
        'et': el.offsetTop,
        'eb': el.offsetTop + el.clientHeight
    }
}

function canseesc(cn,el) { //cn container, el,,, element
    var b = elBounds(cn, el);
    //IMPORTANT NOTE: IF USING THIS FUNCTION PLEASE MAKE SURE THAT THE PARENT IS POSITIONED
    //more info https://developer.mozilla.org/en-US/docs/Web/API/HTMLelement/offsetParent

    return ((b.eb <= b.cb) && (b.et >= b.ct)); //adapted from https://stackoverflow.com/a/488073
}

function canSeeElBounds(tp, cn, el) {
    var b = elBounds(cn, el);
    if(tp) {
        return b.et >= b.ct;
    } else {
        return b.eb <= b.cb;
    }
}

function scrolliv(el,dn,elpr) {
    if(elpr === undefined) {
        elpr = el.parentElement;
    }
    if(!canseesc(elpr,el)) {
        if(dn) { //going down
            el.scrollIntoView(false); //align to bottom
        } else { //going up
            el.scrollIntoView(true); //align to top
        }
    }
}

function objectMerge(mergeInObj, mainObj, overwriteIfDifferent) {
    /* 
        the mainObj will be overwritten by the mergeInObj.

        this operation will be done in place, 
        as in it will modify the objects directly,
        not make a copy and then return the new one to you.
    */
    Object.keys(mergeInObj).forEach((k)=>{
        var overwrite = false;
        if(k in mainObj) {
            if(
                typeof(mainObj[k]) === 'object' &&
                typeof(mergeInObj[k]) === 'object'
            ) {
                objectMerge(
                    mergeInObj[k],
                    mainObj[k],
                    overwriteIfDifferent
                );
            } else if(
                mainObj[k] !== mergeInObj[k] &&
                overwriteIfDifferent
            ) {
                overwrite = true;
            }
        } else {
            /* 
                it will be just assigned to it.
                be careful, if the thing to be assigned is an object,
                it will just be a pointer.
            */
            overwrite = true;
        }

        if(overwrite) {
            mainObj[k] = mergeInObj[k];
        }
    });
}

function lookfor(list,obj) {
    for(var i = 0; i < list.length; i++) {
        if(list[i] === obj) {
            return i;
        }
    }
}

function returnProperty() {
    var mo = arguments[0],
    ka = arguments[1];
    if(!Array.isArray(ka)) {
        ka = [...arguments];
        ka.shift();
    }
    for(var i = 0; i < ka.length; i++) {
        if(ka[i] in mo) {
            mo = mo[ka[i]];
        } else {
            return {
                isIn: false,
                property: undefined
            };
        }
    }
    return {
        isIn: true,
        property: mo
    };
}

function pJSON(r) {
    try {
        return JSON.parse(r);
    } catch(e) {
        console.log(e);
        return;
    }
}

function lazyLoadHandler(llrange,pivot,listCimage) {
    /* 
    llrange = lazyload range
    pivot = origin of lazyload image group
    listCimage = list Containing images
    */
    for(var i = llrange * -1; i < llrange; i++) {
        var cr = pivot;
        if(pivot instanceof HTMLElement) {
            cr = cr.tabIndex;
        }
        cr = cr + i;
        if(cr in listCimage) {
            if(!(listCimage[cr] instanceof HTMLImageElement)) {
                cr = listCimage[cr].getElementsByTagName('img');
                if(0 in cr) {
                    cr = cr[0];
                } else {
                    continue;
                }
            } else {
                cr = listCimage[cr];
            }
            if(cr.dataset.src) {
                cr.src = cr.dataset.src;
                cr.dataset.src = '';
            }
        }
    }
    
}

function numclamp(low, high, num) {
    return Math.max(low, //min
        Math.min(high, //max
            num //num to clamp
        )
    );
}


function alertMessage(message,disptime,type) {
    if(window.parent) {
		window.parent.alertMessage(message,disptime,type)
	}
}
    

var reloadOnHashChange = true;
window.addEventListener('hashchange',()=>{
    if(reloadOnHashChange) {
        location.reload();
    }
});

function updateTheme() {
    var ct = getSettingValue('color-theme'),
    te = eid('color-theme');

    if(te) {
        te.remove();
    }

    if(ct !== 0) {
        var l = document.createElement('link');
        l.rel = 'stylesheet';
        l.type = 'text/css';
        l.id = 'color-theme';
        l.href = '/common/style/theme/' + [
            'shitty-light-theme',
            'prerandomized'
        ][ct - 1] + '.css';
        document.head.appendChild(l);
    }
}
updateTheme();


/* slightly unimportant */

function keyisnav(k) { return ['ArrowDown','ArrowUp','ArrowLeft','ArrowRight'].indexOf(k.key) > -1; }



/*  */
function goToWatchScreenWithPlaylist(listen,plid,vid,ind,userpl){
    var prms = new URLSearchParams();
    prms.append('listen',listen);
    prms.append('playlist', plid);
    prms.append('v',vid);
    prms.append('playlistindex', (ind));
    if(userpl) {prms.append('userPlaylist','1')}
    location = `/watch/index.html#${prms.toString()}`;
};
