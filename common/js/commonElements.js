(function() {

    //minimize dialog
    var md = document.createElement('center'),
    mdicon = document.createElement('img'),
    mdtext = document.createElement('div'),
    mdtextextra = document.createElement('div');
    md.id = 'minimizeDialog'; md.classList.add('hidden','center');
    mdtext.innerHTML = i18n('minimize-dialog');
    mdtextextra.id = 'minimizeDialogCustomText';
    mdicon.src = '/img/minimize.png';
    mdicon.classList.add('icon');
    md.appendChild(mdicon);
    md.appendChild(mdtext);
    md.appendChild(mdtextextra);
    document.body.appendChild(md);        

    //main menu container (this is just the container)
    var mm = document.createElement('div');
    mm.id = 'main-menu-cont';
    mm.classList.add('screen','dim','hidden');
    document.body.appendChild(mm);

    //messagebox
    msgBoxInit();

    //navbar
    var nv = document.createElement('div');
    nv.id = 'navbar';
    ['left','center','right'].forEach((idn)=>{
        var e = document.createElement('div');
        e.id = 'navbar-' + idn;
        e.classList.add(
            'vertical-center'
        );
        nv.appendChild(e)
    });
    document.body.appendChild(nv);

})();