var mainMenuOpts = [
    {n:i18n('home'), a: ()=>{location = '/home/index.html'}},
    {n:i18n('feed'), a: ()=>{goToSearchScreen(null, 'subscriptions')}},
    {n:i18n('subscriptions-menu-item'), a: ()=>{location = '/subscriptions/index.html'}},
    {n:i18n('playlists-menu-item'), a:()=>{goToSearchScreen(null, 'userPlaylistList')}},
    {n:i18n('history'), a:()=>{location = '/history/index.html';}},
    {n: [i18n('search-options')]},
    {n:i18n('search'), a:()=>{location = '/search/index.html';}},
    {n:i18n('video-id'), a:()=>{location = '/watch/index.html';}},
    {n:[i18n('application')]},
    {n:i18n('about-help'), a:()=>{window.open('/about/about.html');}},
    {n:i18n('settings'), a:()=>{location = '/settings/index.html'}},
    {n:i18n('set-instance'), a:()=>{location = '/instance-picker/index.html';}},
    {n:i18n('refresh'), a:()=>{location.reload()}},
    {n:i18n('minimize'), a:()=>{enableMinimize();return 1}},
    {n:i18n('exit'), a:()=>{window.parent.close()}}
],
mainMenuPrevPage = 0, mainMenuPrevFocus = null, mainMenuPrevBackState = null;

placeMainMenu();

function placeMainMenu() {
    var
    cont = document.createElement('div'),
    contcont = document.createElement('div'),
    header = document.createElement('div'),
    ctabi = 0;

    cont.id = 'main-menu'; cont.className = 'option-menu-cont'; contcont.className = 'option-menu';
    header.className = 'menuheader';
    header.textContent = '~ BankiTube ~';
    cont.appendChild(header);

    for(var i = 0; i < mainMenuOpts.length;) {
        var el = document.createElement('div');
        if(Array.isArray(mainMenuOpts[i].n)) {
            el.classList.add('menusubheader');
            el.textContent = mainMenuOpts[i].n[0];
            mainMenuOpts.splice(i,1);
        } else {
            el.classList.add('main-menu-opt','menu-entry', 'focusable-item');
            el.textContent = mainMenuOpts[i++].n; //i is incremented here
            el.tabIndex = ctabi++; //++ will return the previous value.
        }
        contcont.appendChild(el);
    }
    cont.appendChild(contcont);
    eid('main-menu-cont').appendChild(cont);
}

function mainMenuK(k) {
    if(keyisnav(k)) {k.preventDefault()}
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            navMainMenu(u || 1);
            break;
        case 'Enter':
            if(
                (mainMenuOpts[actEl().tabIndex].a || emptyFn)() === 1
            ) {
                break;
            }
            //dont break, continue to hide the main menu
        case 'SoftLeft':
        case 'Backspace':
            hideMainMenu();break;
    }
}


function hideMainMenu(noPgChange){
    if(!noPgChange) {
        curpage = mainMenuPrevPage;
        mainMenuPrevFocus.focus();
        allowBack = mainMenuPrevBackState;
    }
    eid('main-menu-cont').classList.add('hidden');
}
function navMainMenu(nav) {
    var mm = eid('main-menu-cont').getElementsByClassName('main-menu-opt');
    if(nav === 0) {
        eid('main-menu-cont').classList.remove('hidden');

        mainMenuPrevPage = curpage;
        mainMenuPrevFocus = actEl();
        mainMenuPrevBackState = allowBack;

        allowBack = false;
        curpage = 99;
        mm[0].focus();
    } else {
        navigatelist(
            actEl().tabIndex,
            mm, nav
        )
    }
}