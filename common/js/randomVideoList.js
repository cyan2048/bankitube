function randomVideoList() {
    var rvl = [
        //notes are included if there is a /* */ style comment AFTER the id.

        //Internet History
        [
            '44MyrH4jwOQ', //"Mario be playin' T-Dub"-T-Dub (Rap-along Version)
            'rW6M8D41ZWU', //yatta
            'jkKjM1H7ZRE', //Ken Silverman - FLYSONG.KDM
            'ppCDINkI7DQ', //Ken Silverman - MU40PROJ.KDM
            'FuX5_OWObA0', //Rainbow Road
            'KmtzQCSh6xk', //Numa Numa

            //user request
            'jNQXAC9IVRw', /* how did i forget to add this video omg */ //Me at the zoo
            'ee925OTFBCA', //Best Cry Ever
            '_cZC67wXUTs', /* not a big truck. its a series of tubes. */ //Series of Tubes Music Video
            'EwTZ2xpQwpA', /* HOW DID I FORGET THIS ONE TOO OMG */ //"Chocolate Rain" Original Song by Tay Zonday
            'bFEoMO0pc7k', //Sweet Brown - Ain't Nobody Got Time for That (Autotune Remix)
            'ZZ5LpwO-An4', /* whats going on */ //HEYYEYAAEYAAAEYAEYAA
            'Yh0AhrY9GjA', //JB Fanvideo
            'BBGEG21CGo0', //HD Epic Sax Gandalf

        ],
        //musics
        [
            'A0VYsiMtrNE', //電磁祭囃子 in NEO TOKYO 2020
            'bOfpQt4KFCc', //Barcoders Jamming
            '1CAW8AVz498', //【東方MV】進捗どうですか？【IOSYS】
            '9gTBans6tNM', //【BGAP_CU】ニニ【BGA】
            '1RQP8bGWOeE', //【ONE】 恋とキングコング / 日向美ビタースイーツ♪ 【DTXHD】
            '8U1pAknR_FA', //All I Want for Christmas is a Girlfriend
            'lYbP2PMThWM', //29 - Tree of Life
            '47XHUGbVthk', //Germ Killer Music
            'BnTW6fZz-1E', //Cheeki Breeki Hardbass Anthem
            'RcsU6pVJ5SQ', //Last gregg
            'QJXsLiPFpmQ', //ProJared Theme "Goodbye Summer, Hello Winter" by FantomenK (Music Video) // Epic Game Music
            'h-mUGj41hWA', //NOMA - Brain Power - LYRICS!
            'yL7oZy90U0I', //[OverRapid Official GST] Aqua Stars (Sound Souler)
            'o6ruyZtHsAw', //.flow OST: Sugar Hole (Back Rooms) (Extended)
            'OXQBC7ILDr0', //.flow OST: .flowbeat
            'o551Em46-DE', //（:]ミ（:]彡（:]ミ（:]彡
            'J2sWJ1li9T0', //【ふるさとGP】「追憶のアリア」ショートVer. - Blanc Bunny Bandit｜バンめし♪
            '5q0KEnIbb3w', //「Future Funk」tomatoism - Ｓｏｍｅｏｎｅ Ｓｐｅｃｉａｌ
            'ui7DwdypY6k', //Bill Nye the Psytrance Guy
            'H221MRRgFZs', //SERIAL EXPERIMENTS LAIN (game) - main music theme
            'slazi2PpYUo', //Kill Me Baby ED (Full)
            'WHkPimNvuVM', /* YASAKA KANAKO KILLS ME EVERY TIME IN TOUHOU 10 EVERY TIME I SWEAR also i love みぃ's voice alot */ //【GET IN THE RING】AQ - Give your faith!
            'ULeDlxa3gyc', //Harry Enfield - Loadsamoney (Doin' Up the House)
            'ifs4zmWD3ms', //Promise (Get Down) - Hirose Kohmi (HQ Full Original Song Actual Speed)
            'xt4dgc3gs1U', //【少女☆歌劇 レヴュー・スタァライトED Full】（fly me to the star）9人のコーラス
            'HRgjFz50CY0', //「Sound Souler」 Empty Stars
            'impszF10FUk', //Carry Me Away (Extended Mix)
            'lD8zcOlTecg', //capsule - idol fancy
            'cA7oNVFD3ow', //王心凌 - honey 舞蹈完整版
            '9h2jAFCIIwE', //日向美ビタースイーツ♪ - 今夜はパジャマパーティ (Full Ver.)
            '4tT0j_-42Ac', //ツユ - 奴隷じゃないなら何ですか？ MV
            'dcOXDPuRIp8', //The Messenger (not decorated)
            'HrZAuEhCUNA', //Eufonius - Guruguru
            '3kmbeIrBD00', //劇団レコード - 風鈴花火
            'q-n-V9dJLGk', /* Round Table - Friday Night */ //fridey night
            'RIQ8YCK20i0', //MAGICAL GIRL「恋のシャレード ＬＯＮＧ」remastered ver.

            //user request
            '9bZkp7q19f0', /* 2012 vibes... */ //PSY - GANGNAM STYLE(강남스타일) M/V
            'j7_lSP8Vc3o', //2NE1 - 내가 제일 잘 나가(I AM THE BEST) M/V
            'QZBn1e9pr2Q', //Wonder Girls "NOBODY (Kor. Ver)" M/V
            '8nv2wE1nu-E', //PIZZICATO FIVE / スウィート・ソウル・レヴュー
            'oBuLiL3UptI', /* quite painful sound quality. definitely homemade vibes its quite nice */ //Kiyomi Play [귀요미송 by 하리] - Kiyomi Song
            '_LjN3UclYzU', /* personally i never understood the original video. */ //The Missile Knows Where it is (Remix)
            'dQw4w9WgXcQ',
        ],
        //memes/etc
        [
            'RmRZT8QwRi8', //when its your 4th time dying in Bob-Omb Battlefield
            'FDhOIQNwO5I', //Spongebob 2020 Presidential Debate
            'fCmvXCgZr74', //apple bottom jeans, boots with the
            '01wYUOcmDIE', //Witch turns dog into wood 
            'W5g33W0Zafs', //The Wrist Game
            'Wl959QnD3lM', //Wide Putin walking but he's always in frame (full version)
            '2sBFkfcB8WA', //Badlands chugs the ocean
            'rGflu3TbREo', //"YMCA" reworked to Minor key
            '-uAZdIJIl8o', //Nokia ringtone arabic
            'cuYIxUdcS3g', //Steve Harvey in Wii Bowling
            'BLikP6BDH5w', //John Cena "are you sure about that?" GREENSCREEN (IMPROVED VERSION)
            'JjS3wMkOsWE', //YALL IT'S MICHAEL JACKSON!!!
            '-6yhxIdcAdA', //slap chop ad but every chop is the vine boom sound effect
            'Frazx5zxScE', //a normal human reaction to tragedy
            'izISNE_irHA', //sleep tracking app

            //user rq
            '-Zn75eMkaoE', //Happy Monkey Circle
        ],
        //tv stuff
        [
            'maAFcEU6atk', //Tim and Eric - Celery Man
            '6JPcimrnXGA', //US To Trade Gold Reserves For Cash Through Cash4Gold.com
            'MH40LuAmHok', //Oh Please,I have No Soul!
            'WRFmVxpiICc', //Ethan Forhetz reports on 'sketch' of suspected thief
            'WXgNo5Smino', //Live at the Necropolis: Lords of Synth | Adult Swim
            'Vpqffgak7To', //The X files intro and opening theme,
            'LScs78fkWho', //AAAAAAHHHHHHHHHHHH
            'O4VZlbFeq2U', //Spelling professor
        ],
        //short videos (< 5 minutes)
        [
            'yYNrKVqoX1o', //Toddler swing analysis at the Houston Open
            'T28LyXf8MlU', //Give me a drink, bartender
            'Nv9x7E5tnoA', //“OH NO” *fart* (original by h1t1)
            'b3_lVSrPB6w', //You're Correct Horse
            'N70pG4pqqJk', //Backwards Long Jump Gone Wrong
            'NVlqGe5YXfU', //pikopiko-cooking
            'QEzhxP-pdos', //Do I look like I know what a JPEG is?
            'G98KSpP2kQI', //Fumofumo Scan!
            '9oI1fq7taiE', //Sarcastic Weatherman Goes Off
            '4R1WPK3u8II', //Mario Farts!
            'TwIvUbOhcKE', //How NOT to Make an Electric Guitar (The Hazards of Electricity)
            'NOtLb34Osl0', /* Blackboard almost hits Lecturer at German University */ //Tafel stürzt beinahe auf Dozenten | TU München | Elektrotechnik
            '70p3S7MS2U0', //The Bowling Alley Screen When You Get A Strike

            //user request
            'ull5YaEHvw0', //Are you from Russia?
            'rRh3xNpnx2E', //German Hardbass
        ],
        //other
        [
            'wT7gz2YNF4g', //EmpLemon's Downward Spiral. (Template)
            'mR2nM4-ki5A', //Garfield minus Garfield
            'wDP2c2FCHAM', //Mistake Waltz
            '-RFunvF0mDw', //nobody here
            'YkAZy5m46lc', //YouTube for Mobile unveils millions of videos & new Java app
            'oIkhgagvrjI', /* shows an amusing bug in the "ratings" section. no i wont fix that. */ //Why do YouTube views freeze at 301?
        ]
    ],
    
    rvlcats = getSettingValue('random-include-categories'),
    output = [];

    if(getSettingValue('random-include-mature')) {
        var rvlm = [
            //internet history
            [
                'hddCpJ8MfVo', //Some guy yells at some cats
            ],
            //musics
            [
                '_twZEbLQncc', //Trip.swf / PREPSONG.KDM - Genesis Remake
                'xFSwIw-_-as', //Thomas Gon' Give It To Ya

                //user request
                'hMtZfW2z9dw', //BED INTRUDER SONG!!!
            ],
            //memes/etc
            [
                'AhK4nt5we_8', //Stan Twitter: voila what do we have ladies a fucking clown we are a stupid bitch a fucking clown
                'SZqn60_GbbQ', //These Birds Hate People
                '_czzkqPjvS4', //Top 10 N Words
                'WJihC9fLMnc', //Objects that I have shoved up my arse (but it's vocoded)
                'KIyk-bwM3ak', //creator of sex
                'R4GlR6X4ljU', //Oblivion npc conversation
                'y4CIbwB2rbo', //Jump in the CAAC
                '1E1VY4KOghI', //Grab his dick and twist it!! (The old dick twist)

            ],
            //tv clips
            [
                'sR3PW-8p8cA', //Beavis and Butthead - Sex Ed

            ],
            //short videos
            [
                'Qc2R1jDhU5U' /* (voice activated mario phone) */ //MOV03216
            ],
            //other
            [
                'aFdE__2OKc8', //Not so fast
                'k9VewWKfH_0', //[651] Manipulating My Tiny Coq
            ]
        ];
        for(var i = 0; i < rvl.length; i++) {
            rvl[i] = rvl[i].concat(rvlm[i]);
        }
    }

    //the following videos have been deleted (rip)
    //'LBwt4YNPc84', //Masayuki Suzuki — Love Dramatic (Live) {Kaguya-sama wa Kokurasetai OP}
    //'Hp5kUmni5Dk', /* HAPPY BIRTHDAY LAIN!!! 2021-07-06 */ //Serial Experiments Lain OP BD

    //if(rvlcats === []) {rvlcats = [0,1,2,3,4,5];}

    rvlcats.forEach(e => {output = output.concat(rvl[e]);});

    if(output.length === 0) {
        return [
            'EsmEWmWHBro' //(still got no videos)
            //this video always returns if the combination of categories or whatever is empty
        ];
    }
    return output;
}
