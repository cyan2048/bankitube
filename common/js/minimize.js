var minimizeKprevpg = 0, minimizeKpressed = false;

function enableMinimize() {
    allowMinimize = true;
    minimizeKprevpg = curpage;
    eid('minimizeDialog').classList.remove('hidden');
    minimizeKpressed = false;
    curpage = 98;
}

function disableMinimize() {
    allowMinimize = false;
    hideMainMenu();
    eid('minimizeDialog').classList.add('hidden');
}

function minimizeK(k) {
    if(k.key !== 'Backspace' && minimizeKpressed === true) {
        disableMinimize();
    } else {
        minimizeKpressed = true;
    }
}