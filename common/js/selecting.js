function thumbchoose(thumbs,quality) {
    var ifail = '/img/nothumbnail.png';

    if(typeof(quality) === 'number') {
        return imgChoose(thumbs,'height',quality) || ifail;
    } else if(quality === undefined) {
        quality = 'default';
    } //else trust that quality is a string...

    for(var i = 0; i < thumbs.length; i++) {
        if(thumbs[i].quality === quality){
            return thumbs[i].url;
        }
    }
    return ifail;
}
function imgChoose(imgs,dim,size) {
    if(size === undefined) {size = 100}
    if(dim === undefined) {dim = 'height'}
    imgs.sort((a,b)=>{
        return b[dim] - a[dim];
    });
    for(var j = 0; j < imgs.length; j++) {
        if(imgs[j][dim] <= size && !!imgs[j].url){
            return imgs[j].url;
        }
    }
    return false;
}
