var messageBoxData = {};

//page: 95

function msgBoxInit() {
    var scr = document.createElement('div');
    messageBoxData.screen = scr;
    scr.classList.add('screen','dim','hidden');

    var el = document.createElement('div');
    el.classList.add('option-menu-cont');
    messageBoxData.element = el;

    //title
    var header = document.createElement('div');
    header.classList.add('menuheader');
    messageBoxData.titleElement = header;

    var body = document.createElement('div');
    body.classList.add('option-menu', 'message-box', 'scrollable-screen');
    body.tabIndex = -1;
    messageBoxData.bodyElement = body;

    el.appendChild(header);
    el.appendChild(body);
    scr.appendChild(el);
    document.body.appendChild(scr);

    msgBoxInit = undefined;
}

function messageBox(title, body, actions) {
    if(!actions) {
        actions = {};
    }

    if(!messageBoxData.active) {
        messageBoxData.last = {
            page: curpage,
            allowBack: allowBack,
            focus: actEl()
        }
        allowBack = false;
        messageBoxData.active = true;
    }

    if(!actions.back) {
        actions.back = messageBoxOption(messageBoxDefaultBackCallback);
    }

    messageBoxData.currentActions = actions;

    messageBoxData.titleElement.textContent = title;
    messageBoxData.bodyElement.innerHTML = body;

    messageBoxData.screen.classList.remove('hidden');
    messageBoxData.bodyElement.focus();

    messageBoxUpdateNavbar();
    curpage = 95;
}

function messageBoxHide() {
    messageBoxData.screen.classList.add('hidden');
    messageBoxData.bodyElement.innerHTML = '';
    messageBoxData.currentActions = null;
    messageBoxData.last = null;
    messageBoxData.active = false;
}

var messageBoxKConsts = {
    SoftLeft: 'left',
    SoftRight: 'right',
    Enter: 'center',
    Backspace: 'back'
};
function messageBoxK(k) {
    var tmbkc = messageBoxKConsts[k.key],
    ca = messageBoxData.currentActions[tmbkc] //current action
    ;

    if(ca) {
        ca.callback(tmbkc);
        if(!ca.noHide) {
            messageBoxHide();
        }
    }
}
function messageBoxDefaultBackCallback() {
    curpage = messageBoxData.last.page;
    allowBack = messageBoxData.last.allowBack;
    messageBoxData.last.focus.focus();
}
function messageBoxOption(callback, label, noHide) {
    return {
        label: label,
        callback: callback,
        noHide: !!noHide
    }
}
function messageBoxUpdateNavbar() {
    var ca = messageBoxData.currentActions, a = [];
    if(ca) {
        a = [
            ca.left,
            ca.center,
            ca.right
        ];
        for(var i = 0; i < a.length; i++) {
            if(a[i]) {
                a[i] = a[i].label;
            } else {
                a[i] = null;
            }
        }
    }
    outputNavbar(a);
}