
var subscriptionsCheck = window.parent.subscriptionsCheck,
subscriptionsAdd = window.parent.subscriptionsAdd,
subscriptionsUpdateMdata = window.parent.subscriptionsUpdateMdata,
subscriptionsRemove = window.parent.subscriptionsRemove,
channelSubscriptions = window.parent.channelSubscriptions,

subscriptionsRequest = null;
subscriptionsRequestResetVariable();

function subscriptionsRequestResetVariable() {
    subscriptionsRequest = {
        checked: 0,
        total: 0,
        running: 0,
        maxRunning: 0,
        pending: [],
        requestObjects: [], //ONLY USE FOR ABORTING
        aborted: false,
        response: [],
        channelUpdated: {},
        finishFunc: null,
        progressFunc: null
    };
}

function getLatestFromSubscriptions(finishFunc, progressFunc, slots) {
    subscriptionsRequest.finishFunc = finishFunc;
    subscriptionsRequest.progressFunc = progressFunc;

    subscriptionsRequest.aborted = false;

    allowSearchBarFocus = false;
    subscriptionsRequest.response = [];

    channelSubscriptions.keys().then((cids)=>{
        subscriptionsRequest.pending = cids;
        subscriptionsRequest.total = cids.length;
    
        subscriptionsRequest.maxRunning = slots || cids.length;
    
        if(subscriptionsRequest.total === 0) {
            subscriptionsRequest.finishFunc({
                response: [],
                channelUpdated: {}
            });
            return;
        }
    
        for(var i = 0; i < subscriptionsRequest.maxRunning; i++) {
            requestSubscriptionXml();
        }
    });
}

function requestSubscriptionXml() {
    if(
        subscriptionsRequest.running < subscriptionsRequest.maxRunning &&
        subscriptionsRequest.pending.length > 0
    ) {
        var cid = subscriptionsRequest.pending[0],
        crq = new XMLHttpRequest({ mozSystem: true }), 
        elTxt = (be, n) => {return be.getElementsByTagName(n)[0].textContent;};

        subscriptionsRequest.pending.splice(0,1);
        subscriptionsRequest.running++;
    
        crq.onload = (e) => {
            if(e.target.status === 200) {
                e = e.target.response;
                var s = (new DOMParser()).parseFromString(e, 'text/xml'), 
                tn = 'getElementsByTagName',
                tsName = elTxt(s, 'title'), //there are multiple <title>s, but channel name is always first.
                tsId = elTxt(s, 'yt:channelId');

                var vs = s[tn]('entry');
                for(var i = 0; i < vs.length; i++) {
                    var vcur = vs[i], tu = (new Date(elTxt(vcur,'published'))).getTime() / 1000;
                    subscriptionsRequest.response.push(
                        {
                            type: 'videoFromSubscription',
                            name: elTxt(vcur,'title'),
                            description: elTxt(vcur,'media:description'),
                            updated: tu,
                            count: commaSeparateNumber(parseInt(vcur[tn]('media:statistics')[0].attributes.views.textContent)),
                            length: null,
                            author: {
                                name: tsName,
                                id: tsId,
                                subcount: 0
                            },
                            id: elTxt(vcur,'yt:videoId'),
                            images: [vcur[tn]('media:thumbnail')[0].attributes.url.textContent],
                            extra: {}
                        }
                    );

                    var ru = subscriptionsRequest.channelUpdated[tsId] || 0;
                    subscriptionsRequest.channelUpdated[tsId] = Math.max(ru, tu);
                }
                subscriptionsRequest.checked++;
                getLatestFromSubscriptionsDone();
            } else {
                getLatestFromSubscriptionsFailure();
            }
        };

        crq.onerror = getLatestFromSubscriptionsFailure;
        crq.onabort = getLatestFromSubscriptionsFailure;

        crq.open('GET', 'https://www.youtube.com/feeds/videos.xml?channel_id=' + cid);
        crq.send();

        subscriptionsRequest.requestObjects.push(crq);
    }
}

function getLatestFromSubscriptionsAbort() {
    subscriptionsRequest.aborted = true;
    subscriptionsRequest.requestObjects.forEach((e)=>{e.abort();});
}

function getLatestFromSubscriptionsFailure() {
    subscriptionsRequest.checked++;
    getLatestFromSubscriptionsDone();
}

function getLatestFromSubscriptionsDone() {
    subscriptionsRequest.running--;

    if(subscriptionsRequest.aborted) {return}

    if(subscriptionsRequest.checked === subscriptionsRequest.total) {
        subscriptionsRequest.response.sort((a,b) => {
            return b.updated - a.updated;
        });

        subscriptionsRequest.finishFunc({
            response: subscriptionsRequest.response,
            channelUpdated: subscriptionsRequest.channelUpdated
        });

        Object.keys(subscriptionsRequest.channelUpdated).forEach((k)=>{
            channelSubscriptions.setItem(k, subscriptionsRequest.channelUpdated[k]);
        });

        subscriptionsRequestResetVariable();
    } else {
        requestSubscriptionXml();
        subscriptionsRequest.progressFunc(
            subscriptionsRequest.checked,
            subscriptionsRequest.total
        );
    }
}
