var allowMinimize = false, disableControls = true, allowBack = true, disableNavKey = false, curpage = 0;

let keyRemap = {
	q: 'SoftLeft',
	Q: 'SoftLeft',
	w: 'SoftRight',
	W: 'SoftRight'
};
window.addEventListener('keydown', function(k){
	if(k.key in keyRemap) {
		k = {
			key: keyRemap[k.key],
			preventDefault: emptyFn
		};
	}
	globalKeyHandler(k);
});

var keyFns = [];
var navbarFns = [];
var addPage = (function(){
	var reservedPagesStart = 95,
	reservedPagesEnd = 100;
	
	return function(
		keyFn = emptyfn,
		navbarFn = emptyfn
	) {
		var n = keyFns.length;

		if(n === reservedPagesStart) {
			n = reservedPagesEnd;

			keyFns.length = n + 1;
			navbarFns.length = n + 1;
			
			reservedPagesStart = undefined;
			reservedPagesEnd = undefined;
		}

		keyFns.push(keyFn);
		navbarFns.push(navbarFn);
		return n;
	}
})();

function globalKeyHandler(k) {
    if(k.key === 'Backspace' && !allowMinimize) {
        k.preventDefault(); 
        if(allowBack) {
            backHistory();
        }
    }

    if(disableNavKey && keyisnav(k)) {
        k.preventDefault();
    }

    if(disableControls) {return;}

    /* 
    absolute curpages!
    -1  - disabled controls, only allow main menu...
    100 - share menu
    99  - main menu
    98  - minimize dialouge
    97  - user playlist add to dialouge
    96  - user playlist create new one
    95  - generic message box
    */
    switch(curpage) {
        case -1: if(k.key === 'SoftLeft'){navMainMenu(0);}break;
        case 100: shareotK(k); break;
        case 99: mainMenuK(k); break;
        case 98: minimizeK(k); break;
        case 97: userPlaylistK(k); break;
        case 96: newUserPlaylistK(k); break;
        case 95: messageBoxK(k); break;
        default: 
			if(curpage in keyFns) {
				keyFns[curpage](k);
			} else if(
				'keyHandler' in window &&
				keyHandler
			) {
				keyHandler(k);
			}
			break;
    }
    updatenavbar();
}

function updatenavbar() {
    switch(curpage) {
        case -1:
            outputNavbar(
                i18n('menu')
            )
            break;
        case 100:
        case 99:
            outputNavbar(
                i18n('back'), 
                i18n('select'), 
                ''
            ); break;
        case 98: 
        case -100:
            outputNavbar(); break;
        case 97:
        case 96: 
            playlistUpdateNavbar(); 
            break;
        case 95: messageBoxUpdateNavbar(); break;
        default: 
			if(curpage in navbarFns) {
				navbarFns[curpage]();
			} else if(
				'localupdatenavbar' in window &&
				localupdatenavbar
			) {
				localupdatenavbar();
			}
			break;
    }
    //console.log(`updatenavbar: curpage was ${curpage} at this time.`);
    //console.log(updatenavbar.caller);
}