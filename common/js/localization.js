var dateFormatDef;
var timeFormatDef;
var localizationSettings;

function localizationInit() {
    console.log('localization init from', window.location);
	localizationSettings = window.parent.localizationSettings || {};
    dateFormatDef = window.parent.dateFormatDef;
    timeFormatDef = window.parent.timeFormatDef;
	
	if(!('i18n' in window)) {
        window.i18n = window.parent.i18n;
		
		//	style
		//capitalize okay
		document.body.classList.toggle(
			'capitalize-okay',
			( //true/false!?!?!
				(!('capitalizeIsOkay' in localizationSettings)) || //it does not exist in settings, leading to default value
				localizationSettings.capitalizeIsOkay
			)
		)
	}
}

function localizationUpdateObjects(d) {
    if(d) {
		localizationSettings = d.settings;
        i18n.translator.add({
            values: d.translations
        });
        
        sessionStorage.setItem('i18n-data', JSON.stringify(d));
    }

    dateFormatDef = [
        '(y)-(m)-(d)',
        '(y)/(m)/(d)',
        '(y).(m).(d)',
        '(m)-(d)-(y)',
        '(m)/(d)/(y)',
        '(m).(d).(y)',
        '(d)-(m)-(y)',
        '(d)/(m)/(y)',
        '(d).(m).(y)',
        i18n('date-format')
    ][getSettingValue('date-date-format') || 0];
    
    timeFormatDef = [
        '(_hh):(m):(s)', //24h time, zero padded hour.
        '(_h):(m):(s)', //24h time, not zero padded hour.
        '(hh):(m):(s)(af)', //12h time, zero padded hour. am/pm after.
        '(h):(m):(s)(af)', //12h time, not zero padded hour. am/pm after.
        '(af)(hh):(m):(s)', //12h time, zero padded hour. am/pm before.
        '(af)(h):(m):(s)', //12h time, not zero padded hour. am/pm before.

        //no seconds
        '(_hh):(m)', //24h time, zero padded hour.
        '(_h):(m)', //24h time, not zero padded hour.
        '(hh):(m)(af)', //12h time, zero padded hour. am/pm after.
        '(h):(m)(af)', //12h time, not zero padded hour. am/pm after.
        '(af)(hh):(m)', //12h time, zero padded hour. am/pm before.
        '(af)(h):(m)', //12h time, not zero padded hour. am/pm before.

        i18n('time-format')
    ][getSettingValue('date-time-format') || 0];
}

localizationInit();
updateLocalization();

function updateLocalization() {
    var lces = document.getElementsByClassName('localization');

    for(var i = 0; i < lces.length; i++) {
        lces[i].innerHTML = i18n(lces[i].dataset.i18nid);
    }
}