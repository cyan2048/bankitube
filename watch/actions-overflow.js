var videoActionOverflowMenu = new OptionsMenu(i18n('video-actions'));

(()=>{
    var order = [
        'seek-to',
		'speed-control-menu',
        'add-to-playlist',
        'quality-menu',
        'loop-toggle',
        'change-video-fit',
        'toggle-segment-skipping'
    ],
    opts = [ //[action id, i18n id]
        ['seek-to', 'video-action-seek-to'],
        ['add-to-playlist', 'add-to-playlist'],
        ['quality-menu', 'change-quality'],
        ['loop-toggle', 'video-action-loop-toggle'],
        ['change-video-fit', 'video-action-change-video-fit'],
		['speed-control-menu', 'speed-control']
    ];

    //checks
    //segment skipping
    if(segmentSkipping.userCategories.length !== 0) {
        opts.push(['toggle-segment-skipping', '']);
    }

    opts.sort((a,b)=>{
        return (
            order.indexOf(a[0]) -
            order.indexOf(b[0])
        );
    });

    opts.forEach((e)=>{
        var ss = videoActionOverflowMenu.addOption(i18n(e[1]));
        ss.dataset.action = e[0];

        switch(e[0]) { //action id
            case 'toggle-segment-skipping':
                ss.innerHTML = '<span id="segment-skipping-temp-disable-toggle"></span>';
                break;
        }
    });
})();

function showVideoActionOverflowMenu() {
    videoActionOverflowMenu.menuViewToggle(true);
    videoActionOverflowMenu.menu.children[0].focus();
    curpage = 6;
}
function hideVideoActionOverflowMenu(dr) {
    videoActionOverflowMenu.menuViewToggle(false);
    if(!dr){navFocus(5);}
}

function overflowVideoActionsK(k) {
    var dontReturn = false;
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            navigatelist(
                actEl().tabIndex, 
                videoActionOverflowMenu.menu.children, 
                u || 1
            );
            break;
        case 'Enter':
            dontReturn = actionsSelect(actEl().dataset.action) || false;
        case 'SoftLeft':
        case 'Backspace':
            hideVideoActionOverflowMenu(dontReturn);
            break;
    }
}