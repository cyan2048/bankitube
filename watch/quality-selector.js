var qualityMenu = new OptionsMenuSelectable(
    i18n('change-quality'),
    'radio'
),
qualityMenuData = {},

filterMediaQualityKey = {
    video: 'resolution',
    audio: 'bitrate'
};

//menu
function qualityMenuShow() {
    qualityMenu.menuViewToggle(true, 2);
    curpage = 7;
}
function qualityMenuDone() {
    qualityMenu.menuViewToggle(false);
    //return to the actions menu...
    curpage = 2;
    navFocus(5);
}
function qualityMenuK(k) {
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            qualityMenu.navigate(u || 1);
            k.preventDefault();
            break;
        case 'Enter':
            qualityMenu.selectItem(actEl().tabIndex);
            var qk = (listenMode? 'audio' : 'video'),
            nq;

            if(useFormatStreams || listenMode) {
                nq = parseInt(output.mediaAvailable.adaptive[qk][
                    qualityMenu.getValue().value
                ][filterMediaQualityKey[qk]]) / 1000;
            }

            changeQuality(nq);
            exitToMain();
            break;
        case 'Backspace':
        case 'SoftLeft':
            qualityMenuDone();
            break;
    }
}

//logic
function filterMedia(type, mediaList) {
    var allSrcs = [].concat(mediaList), //to copy array.
    qualityObject,
    availSrcs = [];

    /*  ==expected values for quality, depending on type.==
    video: expect a number from "480p"
    audio: expect a bitrate in kbps.
    */

    switch(type) {
        case 'video': 
            for(var i = 0; i < allSrcs.length; i++) {
                if('resolution' in allSrcs[i]) {
                    allSrcs[i].resolution = Number(
                        allSrcs[i].resolution.substring(
                            0,
                            allSrcs[i].resolution.length - 1
                        )
                    );
                }
            }
            break;
        case 'audio': 
            break;
        default: throw 'chooseMedia: invalid type.';
    }
    qualityObject = filterMediaQualityKey[type];

    for(var i = 0; i < allSrcs.length; i++) {
        var cs = allSrcs[i];
        if(
            cs.type.indexOf(type) !== -1 //is the type
            && cs.type.indexOf('webm') === -1
            && !/av0?1/ig.test(cs.type) //not using av1 encoding
        ) {availSrcs.push(allSrcs[i]);}
    }

    availSrcs.sort((a,b) => {return b[qualityObject] - a[qualityObject];});

    console.log(availSrcs);
    return availSrcs;
}

function chooseMedia(type, quality, mediaList) {
    var cmr = {
        url: null,
        quality: null
    },
    qualityObject = filterMediaQualityKey[type];

    switch(type) {
        case 'audio': quality *= 1001; break;
    }

    console.log(`chooseMedia: target is ${quality}.`);

    var i = 0;
    for(; i < mediaList.length; i++) {
        var slql = Number(mediaList[i][qualityObject]);

        cmr.url = mediaList[i].url;
        cmr.quality = slql;

        if(slql <= quality) {
            console.log(`chooseMedia: ${qualityObject}: highest quality is ${slql}`);
            break;
        }
    }
    if(i === mediaList.length) {console.log(`chooseMedia: ${qualityObject}: did not find the target quality, choosing lowest instead (${cmr.quality}).`);}
    return cmr;
}