function renderCaptionsActual(activeCues) {
    var cc = eid('captions-renderer'),
    displayingCaptions = cc.children
    activeCues = !!activeCues? activeCues : [];

    for(var i = 0; i < displayingCaptions.length; i++) {
        displayingCaptions[i].remove();
    }

    for(var i = 0; i < activeCues.length; i++) {
        var cpel = document.createElement('div'),
        cspan = document.createElement('span');

        cspan.textContent = activeCues[i].text;
        cpel.appendChild(cspan);
        cc.appendChild(cpel);
    }
}


(()=>{
    //styles
    var cc = eid('captions-renderer');

    //font size
    cc.style.fontSize = [
        'small',
        null, //normal
        'large',
        'x-large'
    ][getSettingValue('captions-size')];
    
    //colors
    var clrs = [
        'fff',
        '000',
        'f00',
        '0f0',
        '00f',
        '0ff',
        'ff0',
        'f0f'
    ]
    //text color
    cc.style.setProperty(
        '--caption-text-color',
        '#' + clrs[getSettingValue('captions-text-color')]
    );
    //bg color
    //get opacity first.
    var op = getSettingValue('captions-background-opacity') / 4;

    if(op !== 0) { //if op is 0 then dont bother.
        //now form rgba(x,x,x,x);
        var tclr = clrs[getSettingValue('captions-background-color')],
        rgba = [];

        for(var i = 0; i < 3; i++) {
            rgba.push(Math.max((parseInt(tclr.charAt(i), 16) / 0xf) * 255, 0));
        }
        rgba.push(op);
        
        cc.style.setProperty(
            '--caption-bg-color',
            `rgba(${rgba.join(',')})`
        );
    }

    //text edge style
    var tes = getSettingValue('captions-text-edge-style');
    if(tes > 0) {
        cc.classList.add([
            'textoutline',
            'textshadow'
        ][tes - 1]);
    }
})();

function renderCaptions(e){
    renderCaptionsActual(e.target.activeCues);
}

function renderCaptionsClear() {renderCaptionsActual();}
