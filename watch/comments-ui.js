function navigateComments(move) {
    var c = eid('data-comments');
    if(canSeeElBounds(
        move < 0,
        c, 
        actEl()
    )) {
        navigatelist(
            actEl().tabIndex,
            c.children,
            move
        );
        if(
            actEl().clientHeight < c.clientHeight &&
            !canseesc(c, actEl())
        ) {
            actEl().scrollIntoView(move < 0);
        }
        navigateCommentsLazyLoadHandler();
        return true;
    }
    return false;
}

function navigateCommentsLazyLoadHandler() {
    if(commentsImagesEnabled) {
        var pivot = actEl().tabIndex + Number(commentsViewingReply);
        lazyLoadHandler(
            4,
            pivot,
            eid('data-comments').getElementsByClassName('data-comments-img')
        );
    }
}

function navigateCommentsFF() {
	if(commentsExist) {
		var f = 0;
		if(commentsViewingReply) {
			f = 1;
		} else if(commentsLastFocused !== null) {
			f = commentsLastFocused;
			commentsLastFocused = null;
		}
		eid('data-comments').children[f].focus();
		navigateComments(0);
		setTimeout(navigateCommentsLazyLoadHandler);
	}
}

function navigateCommentsCurrentExpandable() {
    return (
        actEl().classList.contains('expandable') &&
        actEl().classList.contains('expandable-' + (checkIfScreenHori()? 'landscape' : 'portrait'))
    );
}
function navigateCommentsUpdateNavbar() {
    var a = [i18n('back'),'',''];

    if(
		(!commentsLoading) &&
		commentsExist
	) {
        switch(actEl().id) {
            case commentsLoadMoreElId:
                a[1] = i18n('load');
                break;
            case commentsReturnToMainElId:
                a[1] = i18n('select');
                break;
            default:
                a[2] = i18n('options');
                if(navigateCommentsCurrentExpandable()) {
                    if(actEl().classList.contains('expanded')) {
                        a[1] = i18n('hide');
                    } else {
                        a[1] = i18n('more');
                    
                    }
                }
                break;
        }
    }
    return a;
}

function navigateCommentsEnter() {
    if(!commentsLoading) {
        var a = actEl();
        switch(a.id) {
            case commentsLoadMoreElId: //get more comments. dont need special logic to check replies or not.
                getComments(commentsReplyCurId);
                break;
            case commentsReturnToMainElId: //go back to main comments
                if(commentsViewingReply) {
                    getComments();
                }
                break;
            default:
                if(navigateCommentsCurrentExpandable()) {
                    if(a.classList.contains('expanded')) {
                        a.classList.remove('expanded');
                        if(!canseesc(eid('data-comments'), a)) {
                            a.scrollIntoView(true); //align to top
                            navigateCommentsLazyLoadHandler();
                        }
                    } else {
                        a.classList.add('expanded');
                    }
                }
                break;
        }
    }
}

//submenu stuff
var commentsSubMenu = new OptionsMenu(i18n('options')),
commentsSubMenuLastFocus;
commentsSubMenu.screen.id = 'comments-sub-menu';

function commentsSubMenuK(k) {
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            commentsSubMenu.navigate(u || 1);
            break;
        case 'Enter':
            var nofocus = false;
            switch(actEl().dataset.id) {
                case 'view-reply':
                    nofocus = true;
                    getComments(
                        commentsSubMenuLastFocus.dataset.id,
                        commentsSubMenuLastFocus.dataset.replyContinuation,
                        commentsSubMenuLastFocus.tabIndex
                    );
                    break;
                case 'view-channel':
                    location = '/channel/index.html#' + commentsSubMenuLastFocus.dataset.channelId;
                    break;
                case 'return-to-main':
                    nofocus = true;
                    getComments();
                    break;
            }
        case 'SoftLeft':
        case 'Backspace':
            closeCommentsSubMenu(nofocus);
            break;
    }
}
function openCommentsSubMenu() {
    if('id' in actEl().dataset) { //is a comment
        curpage = 5;
        commentsSubMenuReprint();
        commentsSubMenuLastFocus = actEl();
        commentsSubMenu.menuViewToggle(true,true);
    }
}
function closeCommentsSubMenu(nofocus) {
    curpage = 1;
    if(!nofocus){commentsSubMenuLastFocus.focus();}
    commentsSubMenuLastFocus = null;
    commentsSubMenu.menuViewToggle(false);
}

function commentsSubMenuReprint() {
    commentsSubMenu.clearMenu();
    var a = actEl();

    if(commentsViewingReply) {
        commentsSubMenu.addOption(
            i18n('return-to-main-comments'),
            'return-to-main'
        );
    } else {
        if('replyContinuation' in a.dataset) {
            commentsSubMenu.addOption(
                i18n('view-replies'),
                'view-reply'
            );
        }
    }

    if(
        'replyContinuation' in a.dataset &&
        !commentsViewingReply
    ) {
        
    }
    commentsSubMenu.addOption(i18n('view-channel'), 'view-channel');
}