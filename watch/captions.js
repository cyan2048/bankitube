var captions, 
captionLoadId, 
captionLoading = false, 
captionsCurrent = -1,
captionsMenu = new OptionsMenuSelectable(
    i18n('video-action-captions-menu'),
    'radio'
);

function loadCaption(id) {
    captionLoadId = id;
    freedatagrab(
        rqApi.base + captions[id].url,
        loadCaptionDone,
        loadCaptionFail,
        false,
        false
    );

    alertMessage(i18n('captions-loading'),5000,0);
}
function loadCaptionDone(captText) {
    //replacing borked vtt file (ms only having presision of 2)
    //initial fix credit: cyan, minhduc_bui1
    captText.replace(
        //make sure not to match "on purpose" times.
        /\n((?:\d{2}[\.:]){3})(\d{2,3}) --> ((?:\d{2}[\.:]){3})(\d{2,3})\n/g,
        (whole, t1t, t1ms, t2t, t2ms)=>{
            if(t1ms.length === 2) {t1ms += '0'}
            if(t2ms.length === 2) {t2ms += '0'}
            return `\n${t1t}${t1ms} --> ${t2t}${t2ms}\n`;
        }
    );

    captions[captionLoadId].data = new Blob([captText]);
    captions[captionLoadId].dataUrl = URL.createObjectURL(captions[captionLoadId].data);

    var e = captions[captionLoadId], trk = document.createElement('track');
    trk.kind = 'captions';
    trk.label = e.label;
    trk.id = captionLoadId;
    trk.src = URL.createObjectURL(captions[captionLoadId].data);

    trk.onerror = (e)=>{
        console.error(e);
        alertMessage(i18n('captions-loaded-invalid'),5000,3);
        captions[captionsCurrent].data = null;
        captionsMenu.selectItem(0);
        captionsCurrent = -1;
    }
    output.video.appendChild(trk);
    setTimeout(()=>{selectCaption(captionLoadId);},1);
    alertMessage(i18n('captions-loaded'),1000,0);
}

function loadCaptionFail() {
    alertMessage(
        i18n('captions-load-fail', {lang: captions[captionLoadId].label}),
        5000,3
    );

}

function selectCaption(index) {
    var vt = output.video.textTracks;
    for(var i = 0; i < vt.length; i++) {
        vt[i].mode = 'hidden';
        vt[i].removeEventListener('cuechange',renderCaptions);
    }
    captionsCurrent = index;
    if(index === -1) {
        renderCaptionsClear();
    } else {

        if('data' in captions[index]) {
            initialCaptionRender();
        } else {
            loadCaption(index);
        }
    }
}

function initialCaptionRender() {
    var c = output.video.textTracks.getTrackById(captionsCurrent);
    if(c) {
        c.addEventListener('cuechange',renderCaptions);
        renderCaptionsActual(c.activeCues);
        console.log(`initialCaptionRender: showed the captions for ${c.label}`);
    } else {
        setTimeout(initialCaptionRender,100);
    }
}

function attachCueChangeHandler() {
    output.video.textTracks.getTrackById(captionsCurrent)
        .addEventListener('cuechange',renderCaptions);
}

function captionK(k) {
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            captionsMenu.navigate(u || 1);
            k.preventDefault();
            break;
        case 'Enter': 
            selectCaption(actEl().tabIndex - 1);
            captionsMenu.selectItem(actEl().tabIndex);
            //dont break, continue to exit action.
        case 'SoftLeft':
        case 'Backspace':
            captionMenuShow(false);break;
    }
}

var captionLastEl = null;
function captionMenuShow(tr) {
    if(captionLoading) {
        alertMessage(i18n('captions-wait-for-previous'),5000,0);
        return;
    }
    
    if(tr) {
        if(captions.length === 0) {
            alertMessage(i18n('captions-not-available'),5000,0);
            return;
        }
        captionLastEl = actEl();
        captionsMenu.menuViewToggle(
            true,
            2
        );
        curpage = 4;
    } else {
        captionLastEl.focus();
        captionLastEl = null;
        captionsMenu.menuViewToggle(false);
        curpage = 2;
    }
}
