var lcHash, 
videoId, 
channelId, 
channelName,
listenMode = false, 
continueTime = 0, 
askContinueTime = false,
forceAutoPlay = false,
useFormatStreams = (getSettingValue('use-format-streams') === 1),
videoLength,
noRelateds = true,
playlistId,
playlistIndex = 0,
playlistItems = [];

function init() {
    curpage = -1;
    updatenavbar();
    disableControls = false;
    allowBack = true;

    forceAutoPlay = false;

    tallVideo = true;
    noRelateds = true;
    eid('actions-vertical-positioner').classList.add('vertical-center');
    eid('actions-relatedvids-cont-wt').classList.add('hidden');
    eid('actions-relatedvids-loading-placeholder').classList.add('hidden'); 

    resetComments();

    abortGetAltRatingData();

    removeMediaElements();
	
	speedControl.reset();

    showErrorScreen(false);
    eid('data-extraAlert').innerHTML = '';

    eid('data-rating').classList.remove('hidden');
    eid('data-rating-description-all-container').classList.remove('disabledRatings');

    output.videoTime.text.textContent = '00:00';
    output.videoTime.textSub.textContent = '00:00';
    output.videoTime.bar.style.width = null;

    eid('age-restricted-indicator').classList.add('hidden');
    eid('unlisted-video-indicator').classList.add('hidden');
    eid('loop-indicator').classList.add('hidden');
    updateStatusIcon('wait');

    output.title.textContent = i18n('loading');
    output.author.textContent = i18n('nanashi');
    output.data.published.textContent = '~';
    output.data.viewcount.textContent = '--';
    output.data.rating.like.textContent = '--';
    output.data.rating.dislike.textContent = '--';
    output.data.rating.altRatingDataNote.classList.add('hidden');


    lcHash = location.hash.substr(1);

    if(lcHash) {
        var prms = new URLSearchParams(lcHash), 
            plm = prms.get('playlist'); 
        videoId = prms.get('v');
        listenMode = prms.get('listen') === 'true';
        continueTime = Number(prms.get('t'));
        askContinueTime = prms.get('askUserBeforeSeek') === 'true';
        playlistMode = !(plm === '' || plm === null);
        userPlaylistMode = prms.get('userPlaylist');
        
        histExData = {};

        if(playlistMode) {histExData.playlist = playlistId;}

        reloadOnHashChange = false;
        pushHistory(videoId,'video',videoId, histExData);
        pushToAllHistory(videoId,'video');

        switch((playlistMode << 1) + !!videoId) {
            case 3:
                playlistId = plm;
                loadPlaylistInit();
                //break;
            case 1:
                loadVideo(videoId);
                break;
            default:
                console.error('init: unhandled combination.');
        }
    } else {
        promptVideo();
        videoLoaderShow(true,i18n('please-wait'));
        pushHistory('dummy');
    }
}

window.addEventListener('DOMContentLoaded',()=>{
    if(getSettingValue('sekibanki-loading-icon') === 0) {
        ecls('bankihead-loader')[0].remove();
    }

    if(getSettingValue('video-time-display') === 2) {
        eid('videoTimeTextSub').classList.remove('hidden');
    }

    if(getSettingValue('video-always-show-time') === 1) {
        document.body.classList.add('minitimebar');
    }

    if(!getSettingValue('listen-mode-show-listen-mode-on-thumbnail')) {
        eid('audioModeDisplay').classList.add('notext');
    }

    if(getSettingValue('listen-mode-blurry-thumbnail')) {
        eid('audioModeDisplay-img').classList.add('blur');
    }
    
    init();
    console.log('loaded');
    }
);

function loadVideo(id) {
    var cachedVid = itemCache('video', id, 'get');
    if(cachedVid) {
        console.log('loadVideo: got data from cache instead.');
        initRqDone(cachedVid);
    } else {
        requestItem(
            'videos',
            id,
            '',
            (r)=>{
                itemCache('video', videoId, 'update', r);
                initRqDone(r);
            },
            initRqError,
            false,
            true
        );    
    }

    getSkipSegments();

    videoLoaderShow(true,i18n('loading'));
}
function loadAnotherVideo(id) {
    var prms = new URLSearchParams();
    prms.append('v',id);
    prms.append('listen',listenMode);
    if(playlistMode) {
        prms.append('playlist',playlistId);
    }
    location.hash = prms.toString();
    exitToMain();
    init();
}
var output = {
    title: eid('data-title'),
    fullTitle: eid('data-fullTitle'),
    author: eid('data-author'),
    fullAuthor: eid('data-fullAuthor'),
    description: eid('data-description'),
    videoCont: eid('videoCont'),
    video: null,
    audio: null,
    mediaLoaded: [0,0], //loaded / expected
    mediaAvailable: null,
    authorPic: eid('actions-video-channel'),
    videoTime: {
        bar: eid('videoTimeIn'),
        text: eid('videoTimeTextMain'),
        textSub: eid('videoTimeTextSub')
    },
    data: {
        published: eid('data-published'),
        publishedDesc: eid('data-fullpublished'),
        viewcount: eid('data-viewcount'),
        viewcountDesc: eid('data-fullviewcount'),
        videoId: eid('data-videoId'),
        category: eid('data-category'),
        runtime: eid('data-runtime'),
        thumbnail: eid('data-thumbnail'),
        rating: {
            like: eid('data-rating-like'),
            dislike: eid('data-rating-dislike'),
            likePercent: eid('data-rating-like-percent'),
            votePercent: eid('data-rating-voted-percent'),
            likePercentBar: eid('data-rating-like-percent-bar'),
            dislikePercentBar: eid('data-rating-dislike-percent-bar'),
            altRatingDataNote: eid('data-rating-alt-rating-data-note')
        }
    }
}
;

function initRqError(e) {
    console.log(e);
    try {
        e = JSON.parse(e.response);
    } catch(e) {}

    output.title.textContent = `${i18n('error')} [ID: ${videoId}]`;
    updateStatusIcon('warning');

    var eo = '';

    if(typeof(e) !== 'string') {
        if('error' in e) {
            switch(e.error) {
                case 'Sign in if you\'ve been granted access to this video': 
                    eo = i18n('video-screen-error-private'); break;
                default: eo = e.error; break;
            }
        } else {
            eo = i18n('unknown-error');
        }
    } else {
        switch(e.status) {
            case 403:
                eo = i18n('video-screen-error-403');
                break;
            case 500:
                eo = i18n('video-screen-error-500');
                break;
            default:
                eo = i18n('video-screen-error-nnn', {code: e.status});
                break;
        }
    }

    showErrorScreen(eo);

    if(playlistMode) {
        playlistNextVideoDTE();
    }
}

function initRqDone(rqResp) {
    var histExData = {};
    if(playlistMode) {histExData.playlist = playlistId;}
    pushHistory(videoId,'video',rqResp.title,histExData);
    //pushToAllHistory(videoId,'video',rqResp.title);
    var videoThumbnail = thumbchoose(rqResp.videoThumbnails);
    mdataStoreSet(
        videoId,
        {
            name: rqResp.title,
            type: 'video',
            author: {
                name: rqResp.author,
                id: rqResp.authorId
            },
            time: rqResp.published,
            length: rqResp.lengthSeconds,
            image: videoThumbnail
        }
    );

    videoId = rqResp.videoId;
    localStorage.setItem('recentVideo',videoId);
    channelId = rqResp.authorId;
    videoLength = rqResp.lengthSeconds;
    captions = rqResp.captions;

    if('error' in rqResp) {
        if (rqResp.error !== 'Could not extract video info. Instance is likely blocked.') {
            eid('videoErrorText').textContent = rqResp.error;
            console.warn(`initRqDone: special error!!!! "${rqResp.error}"`);
        }
        postExtraAlert(rqResp.error, 'border-color: red');
    }

    output.title.textContent = rqResp.title;
    output.fullTitle.textContent = rqResp.title;
    output.author.textContent = rqResp.author;
    output.fullAuthor.textContent = rqResp.author;
    channelName = rqResp.author;
    eid('actions-video-channel-label').dataset.label = rqResp.author;

    output.authorPic.src = imgChoose(rqResp.authorThumbnails,'width',100) || '/img/nouserimage.png';

    subscriptionsUpdateMdata(
        rqResp.authorId,
        rqResp.author,
        imgChoose(rqResp.authorThumbnails,'width',100)
    );
    

    if(rqResp.description.length !== 0) {
        output.description.innerText = rqResp.description;
    } else {
        var ndd = document.createElement('div');
        ndd.classList.add('center', 'text-center');
        ndd.textContent = i18n('video-info-no-description');
        output.description.innerHTML = '';
        output.description.appendChild(ndd);
        ndd = undefined;
    }

    output.data.published.textContent = unixdateformat(rqResp.published);
    output.data.publishedDesc.textContent = unixdateformat(rqResp.published);
    output.data.runtime.textContent = timeformat(rqResp.lengthSeconds);
    output.data.category.textContent = commaSeparateNumber(rqResp.genre);
    output.data.videoId.textContent = videoId;
    output.data.thumbnail.src = videoThumbnail;

    if(rqResp.allowRatings) {
        printRatings(
            rqResp.likeCount,
            null,
            //rqResp.dislikeCount,
            rqResp.viewCount
        );
        if(getSettingValue('alt-rating-data-enabled') === 1) {
            getAltRatingData();
        }
    } else {
        printRatings(false);
    }
    
    if(!rqResp.isFamilyFriendly) {
        eid('age-restricted-indicator').classList.remove('hidden');
        postExtraAlert(i18n('video-info-age-restricted'), 'border-color: #ff0');
    }

    if(!rqResp.isListed) {
        eid('unlisted-video-indicator').classList.remove('hidden');
        postExtraAlert(i18n('video-info-unlisted'));
    }

    output.mediaAvailable = {
        adaptive: {
            video: filterMedia('video', rqResp.adaptiveFormats),
            audio: filterMedia('audio', rqResp.adaptiveFormats)
        },
        format: rqResp.formatStreams
    };

    var targetQuality;

    //quality selector
    qualityMenu.clearMenu();
    qualityCheck: {
        var maak;
        if(listenMode) {
            maak = 'audio';
            targetQuality = Infinity;
        } else {
            if(!useFormatStreams) {
                qualityMenu.addOption('360p', true);
                break qualityCheck;
            }

            maak = 'video';
            targetQuality = chooseMedia(
                'video',
                [
                    144,
                    240,
                    360,
                    480
                ][getSettingValue('default-video-quality')],
                output.mediaAvailable.adaptive.video
            ).quality;
        }

        output.mediaAvailable.adaptive[maak].forEach((q, i) => {
            var ql, iq;
            if(listenMode) {
                ql = Math.floor(q.bitrate / 1000) + 'kbps';
                iq = (i === 0);
            } else {
                ql = q.qualityLabel;
                iq = (q.resolution === targetQuality);
            }

            qualityMenu.addOption(ql, iq);
        });
    }

    //put the video there
    mediaInit(targetQuality);

    //captions
    selectCaption(-1);
    renderCaptionsClear();
    
    captionsMenu.clearMenu();
    captionsMenu.addOption(
        i18n('disabled'),
        true
    );
    captions.forEach((e) => {
        captionsMenu.addOption(e.label);
    });

    //place recommendeds
    if(!playlistMode) {
        if(
			rqResp.recommendedVideos.length !== 0 &&
			!getSettingValue('hide-related-videos')
		) {
            outputRecommends(rqResp.recommendedVideos);
        }
    }

    if(listenMode) {
        eid('minimizeDialogCustomText').textContent = '';

        eid('audioModeDisplay').classList.remove('hidden');
        eid('audioModeDisplay-img').style.backgroundImage = `url(
            ${thumbchoose(rqResp.videoThumbnails, 'medium')}
        )`;
        eid('listen-button').src = '/img/buttons/eye.png';

        document.body.classList.add('audiomode');
    } else {
        eid('minimizeDialogCustomText').textContent = i18n('minimize-listen-mode-hint');
    }

    if(curpage === 99) {hideMainMenu();}
    allowBack = false;

    curpage = 0;
    updatenavbar();
}

function mediaInit(quality) {
    //init
    output.mediaLoaded = [0,0];
    alraedyBeganPlaying = false;
    videoAbleToPlay = false;
    
    if(listenMode) {
        output.video = document.createElement('audio');
		
        let src = chooseMedia(
            'audio', 
            quality, 
            output.mediaAvailable.adaptive.audio
        ).url;
		[
			src,
			getUrlWithHostToApi(src)
		].forEach((url)=>{
			addSourceToMediaElement(
				output.video,
				url
			);
		});
		
        output.mediaLoaded[1] = 1;
    } else {
        eid('minimizeDialogCustomText').textContent = i18n('minimize-listen-mode-hint');

        output.video = document.createElement('video');

        //0 = simple, 1 = "advanced"
		rqApi.base
        if(useFormatStreams) {
            //new behavior
            output.audio = new Audio(); //note: video already added
			//initial sources
            var audioSrc = new URL(chooseMedia(
                'audio', 
                50, 
                output.mediaAvailable.adaptive.audio
            ).url);
            var videoSrc = new URL(chooseMedia(
                'video', 
                quality, 
                output.mediaAvailable.adaptive.video
            ).url);
            output.video.volume = 0;
			
			//setting main and fallback sources
			[
				{url:audioSrc, isFor:1},
				{url:getUrlWithHostToApi(audioSrc), isFor:1},
				{url:videoSrc, isFor:0},
				{url:getUrlWithHostToApi(videoSrc), isFor:0},
			].forEach((entry)=>{
				addSourceToMediaElement(
					output[
						[
							'video',
							'audio'
						][entry.isFor]
					],
					entry.url
				);
			});
			
			//set counter
            output.mediaLoaded[1] = 2;
        } else {
            //old behavior
            var allSrc = output.mediaAvailable.format;
            for(var i = 0; i < allSrc.length; i++) {
                if(allSrc[i].resolution === '360p') {
					//setting multiple sources
					let origUrl = allSrc[i].url;
					[
						origUrl,
						getUrlWithHostToApi(origUrl)
					].forEach((url)=>{
						addSourceToMediaElement(
							output.video,
							url
						);
					});
                    break;
                }
            }
            output.mediaLoaded[1] = 1;
        }
    }
    output.video.id = 'video';

    output.videoCont.appendChild(output.video);

    (output.audio || output.video).mozAudioChannelType = 'content';

    //set events
    output.video.onplay = videoEvents.play;
    output.video.onpause = videoEvents.pause;
    output.video.onplaying = videoEvents.playing;
    output.video.onseeking = videoEvents.seeking;
    output.video.onseeked = videoEvents.seeked;
    output.video.onstalled = videoEvents.stalled;
    output.video.onwaiting = videoEvents.waiting;
    output.video.onerror = videoEvents.error;
    output.video.onended = videoEvents.ended;
    output.video.ontimeupdate = videoEvents.timeupdate;
    output.video.oncanplay = videoEvents.canplay;
    output.video.onloadedmetadata = videoEvents.loadedmetadata;
	output.video.onratechange = videoEvents.ratechange;

    output.video.querySelector('source:last-child').onerror = videoEvents.srcError;

    output.video.preload = 'metadata';

    videoLoaderShow(true,i18n('video-buffering'));

    if(output.audio) {
        output.audio.oncanplay = videoEvents.canplay; //videoevnet func is same as audio
        output.audio.onloadedmetadata = videoEvents.loadedmetadata; //same as above
    
        output.audio.preload = 'metadata';
    }
}

function printRatings(likeOrDisabled, dislikes, viewCount) {
    var percents = [];
    if(likeOrDisabled === false) {
        eid('data-rating').classList.add('hidden');
        eid('data-rating-description-all-container').classList.add('disabledRatings');
    } else {
        var totalVotes = (likeOrDisabled + dislikes);
        percents = [
            ((likeOrDisabled / totalVotes) * 100),
            ((totalVotes / viewCount) * 100)
        ];

        var likeText = (likeOrDisabled === null)? '--' : commaSeparateNumber(likeOrDisabled),
        dislikeText = (dislikes === null)? '--' : commaSeparateNumber(dislikes);

        output.data.rating.like.textContent = likeText;
        output.data.rating.likePercentBar.textContent = likeText;
        output.data.rating.dislike.textContent = dislikeText;
        output.data.rating.dislikePercentBar.textContent = dislikeText;
		
		viewCount = commaSeparateNumber(viewCount);
		output.data.viewcount.textContent = viewCount;
		output.data.viewcountDesc.textContent = viewCount;
    }
    
    percents.forEach((e,i,a) => {
        if(isNaN(e)) {
            a[i] = null;
        } else {
            a[i] = a[i].toFixed(2);
        }
    });
    
    output.data.rating.likePercent.textContent = i18n('video-info-like-percent', {percent: percents[0] || '--.--'});
    output.data.rating.votePercent.textContent = i18n('video-info-voters-percent', {percent: percents[1] || '0.00'});
    output.data.rating.likePercentBar.style.width = (percents[0] || '50') + '%';
}

function removeMediaElements() {
    if(output.video) {
        output.video.onplay = null;
        output.video.onpause = null;
        output.video.onplaying = null;
        output.video.onseeking = null;
        output.video.onseeked = null;
        output.video.onstalled = null;
        output.video.onwaiting = null;
        output.video.onerror = null;
        output.video.onended = null;
        output.video.ontimeupdate = null;
        output.video.oncanplay = null;
        output.video.onloadedmetadata = null;
		output.video.onratechange = null;
    
        output.video.pause();
        output.video.remove();
        output.video = null;
    
        if(output.audio) {
            output.audio.oncanplay = null;
            output.audio.onloadedmetadata = null;
            output.audio.pause();
            output.audio = null;
        }
    }
}

function switchListenMode() {
    var prms = new URLSearchParams(lcHash);
    if(prms.get('listen') === 'true') {
        prms.delete('listen');
    } else {
        prms.set('listen','true');
    }

    prms.set('t', Math.max(0, output.video.currentTime - 1));
    prms.delete('askUserBeforeSeek');
    prms.delete('userPlaylistMode');
    location.hash = prms.toString();
    location.reload();
}

function changeQuality(nq) {
    forceAutoPlay = true;
    alraedyBeganPlaying = false;
    videoAbleToPlay = false;
    continueTime = output.video.currentTime;
    removeMediaElements();
    showErrorScreen(false);
    mediaInit(nq);
}