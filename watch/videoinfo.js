var infoTab = 0, 
infoTabList = [
    {
        el: eid('data-metadata'),
        id: 'metadata',
        i18n: 'metadata'
    },
    {
        el: eid('data-description'),
        id: 'description',
        i18n: 'description'
    },
    {
        el: eid('data-comments'),
        id: 'comments',
        i18n: 'video-info-comments'
    }
],
infoTabDisplay;

document.addEventListener('DOMContentLoaded',()=>{
    infoTabDisplay = new Tabs(false, eid('data-tabs-display'));
    infoTabList.forEach((l)=>{
        infoTabDisplay.addTab(
            i18n(l.i18n),
            l.id
        );
    });
});

function videoInfoVisible() {
    return !eid('data-moreinfo').classList.contains('hidden');
}

function toggleVideoInfo(force) {
    var mn = eid('data-moreinfo');
    if(force === undefined) {
        force = !videoInfoVisible();
    }
    if(force) {
        mn.classList.remove('hidden');
        eid('data-metadata').focus();
        curpage = 1;
    } else {
        mn.classList.add('hidden');
        mn.blur();
        curpage = 0;
    }
    infoTab = 0;
    videoInfoTabFocus(0, true);
}

function videoInfoTabFocus(r,abs) {
    //r = nav right
    if(abs) {
        infoTab = r;
    } else {
        infoTab += -1 + (2 * r);
        if(!(infoTab in infoTabList)) {
            infoTab = (infoTabList.length - 1) * !r;
        }
    }
    infoTabDisplay.focusTab(infoTab);
    infoTabList.forEach((e)=>{
        e.el.classList.add('hidden');
    })
    infoTabList[infoTab].el.classList.remove('hidden');
    infoTabList[infoTab].el.scrollTo(0,0);

    switch(infoTabList[infoTab].id) {
        case 'comments':
            if(commentsLoaded) {
                navigateCommentsFF();
            } else {
                getComments();
            }
            break;
        default:
            infoTabList[infoTab].el.focus();
            break;
    }
}

function descriptionK(k) {
    ms: switch(k.key) {
        case 'SoftLeft':
        case 'Backspace':
            toggleVideoInfo();
            break;
        case '2': actEl().scrollTop = 0; break;
        case '5': actEl().scrollTop -= actEl().clientHeight; break;
        case '8': actEl().scrollTop += actEl().clientHeight; break;
        case '0': actEl().scrollTop = actEl().scrollHeight; break;
        case 'ArrowLeft':  videoInfoTabFocus(false); k.preventDefault(); break;
        case 'ArrowRight': videoInfoTabFocus(true);  k.preventDefault(); break;
        case 'ArrowDown':
        case 'ArrowUp':
            switch(infoTabList[infoTab].id) {
                case 'comments':
                    if(
                        navigateComments(
                            1 - (2 * (k.key.substr(4).indexOf('Up') !== -1))
                        )
                    ) {
                        k.preventDefault();
                    }
                    break;
                default:
                    break ms;
            }
            break;
        case 'Enter':
            switch(infoTabList[infoTab].id) {
                case 'comments':
                    navigateCommentsEnter();
                    break;
            }
            break;
        case 'SoftRight':
            switch(infoTabList[infoTab].id) {
                case 'comments':
                    openCommentsSubMenu();
                    break;
            }
            break;
    }
}
