var segmentSkipping = {
    toSkip: [],
    getting: false,
    overrideDisabled: false,
    userCategories: getSettingValue('sponsorblock-categories'),
    autoSkip: getSettingValue('segment-auto-skip'),
    timebarLabeled: false
}
;

function getSkipSegments() {
    var skdtb = eid('segment-skipping-temp-disable-toggle');
    if(skdtb) {
        skdtb.textContent = i18n('menu-option-disable-segskip');
    }

    if(segmentSkipping.getting) {segmentSkipping.getting.abort();}
    var p = new URLSearchParams(),
    sbc = [
        'sponsor',
        'selfpromo',
        'interaction',
        'intro',
        'outro',
        'preview',
        'music_offtopic'
    ],
    sbcu = []
    ;
    if(segmentSkipping.userCategories.length === 0) {return}

    segmentSkipping.toSkip = [];
    segmentSkipping.overrideDisabled = false;
    segmentSkipping.timebarLabeled = false;
    eid('videoTimeSegmentSkipping').innerHTML = '';

    var csd = itemCache( //cached skip data
        'segmentSkip',
        videoId,
        'get'
    );

    if(csd) {
        getSkipSegmentsDone(csd);
    } else {
        p.append('videoID',videoId);

        segmentSkipping.userCategories.forEach((e)=>{sbcu.push(sbc[e]);});
        p.append('categories', JSON.stringify(sbcu));
    
        segmentSkipping.getting = freedatagrab(
            getSettingValue('sponsorblock-instance') + '/api/skipSegments?' + p.toString(),
            (r)=>{
                itemCache(
                    'segmentSkip',
                    videoId,
                    'update',
                    r
                );
                getSkipSegmentsDone(r);
            },
            getSkipSegmentsFail,
            false,
            true
        );
    }
}

function toggleSegmentSkipping() {
    if(segmentSkipping.userCategories.length !== 0) {
        segmentSkipping.overrideDisabled = !segmentSkipping.overrideDisabled;
        if(segmentSkipping.overrideDisabled) {
            alertMessage(i18n('segment-skipping-disabled'), 3000, 0);
            eid('segment-skipping-temp-disable-toggle').textContent = i18n('menu-option-enable-segskip');
        } else {
            alertMessage(i18n('segment-skipping-enabled'), 3000, 0);
            eid('segment-skipping-temp-disable-toggle').textContent = i18n('menu-option-disable-segskip');
        }
    }
}

function checkSegmentToSkip(tim,returnAll) {
    if(!segmentSkipping.overrideDisabled) {
        var ss = segmentSkipping.toSkip;
        for(var i = 0; i < ss.length; i++) {
            if(ss[i][1] > tim) { //are we before this segment's end?
                if(ss[i][0] < tim) { //are we after this segment's beginning?
                    if(returnAll) {
                        return ss[i];
                    } else {
                        return ss[i][1];
                    }
                }
            }
        }
    }
    return false;
}

function skipSegmentManual() {
    var s = checkSegmentToSkip(output.video.currentTime);
    if(s !== false) {
        output.video.currentTime = s;
    }
}

function getSkipSegmentsDone(r) {
    segmentSkipping.getting = false;
    r.forEach((e)=>{segmentSkipping.toSkip.push(e.segment);});
    console.log(`skip segments: there are ${segmentSkipping.toSkip.length} segment(s) to skip in this video.`);

    if(videoAbleToPlay) {
        labelTimeBarWithSkipSegments();
    }
}

function labelTimeBarWithSkipSegments() {
    if(segmentSkipping.toSkip.length !== 0) {
        var s = segmentSkipping.toSkip, d = output.video.duration;
        eid('videoTimeSegmentSkipping').innerHTML = '';
        
        s.forEach((e)=>{
            var l = document.createElement('div');
            l.style.width = (((e[1] - e[0]) / d) * 100) + '%';
            l.style.left = ((e[0] / d) * 100) + '%';
            eid('videoTimeSegmentSkipping').appendChild(l);
        });

        segmentSkipping.timebarLabeled = true;
    }
}

function getSkipSegmentsFail(r) {
    segmentSkipping.getting = false;
    console.warn('skip segments: video does not have any segments to skip.');
}