function promptVideo() {
    var id=window.prompt(i18n('video-id-prompt'));
    console.log(id);
    if(id === null) {
        backHistory(); return;
    } else if(id === '') {
        id = randomVideoList()[Math.floor(Math.random() * randomVideoList().length)];
    }
    loadAnotherVideo(id);
}

