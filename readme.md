# BankiTube
an alternative youtube player via invidious for kaios.

it is named after a scary ~~and cute~~ character. her head can come off, and she can have more than one head. it's sekibanki!!

this repository along with many others on my profile was created so that people can try out my applications before it is stable.

## download
### stable version
website: https://alego.web.fc2.com/kaiosapps/bankitube/  
bh store: https://store.bananahackers.net/#bankitube  
gitlab release: https://gitlab.com/ale4710/bankitube/-/releases (download the source code. if you dont know which one choose zip.)

### "nightly" version
click the download button above the file list. the download button is next to the button that says "Clone". again if you dont know which one choose zip.

## translation
if you want to translate, navigate to /common/lang/ and read the readme file.

### translators
Russian (русский) - [Pavel Gubanov](https://t.me/my_id_gp)

Korean (한국어) - [Minyeong](https://twitter.com/Min_0_Y)

Bulgarian (български) - [iGameEveryDay06#9822](https://discord.com/users/505459230577524737/)

Italian (Italiano) - [LolloDev5123](https://github.com/LolloDev5123)

German (Deutsch) - [**perry**](https://discord.com/users/520349553124311052/)

## attribution
[localforage](https://github.com/localForage/localForage) by Mozilla. licenced under Apache Licence.

[i18njs](https://github.com/roddeh/i18njs) by roddeh. licenced under MIT.

[drawing of sekibanki](https://twitter.com/i/web/status/932461586161188864) by [@op_na_yarou](https://twitter.com/op_na_yarou). this is temporary and will be changed with another picture of sekibanki later.

youtube/google team - i used [their design of the youtube app for feature phones](https://youtube.com/watch?v=YkAZy5m46lc&t=228) as a reference. you may see some resemblance between their app and my app.

[Return Youtube Dislikes](https://returnyoutubedislike.com)

[SponsorBlock](https://sponsor.ajay.app/)

## licence
as of 2022-03-20, bankitube is licenced under the BSD 3-Clause License.

basically, you can do whatever you want, but you have to give me credit.
