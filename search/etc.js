function localupdatenavbar() {
    var ntext = [i18n('menu'),'',''];
    if(!disableControls) {
        switch(curpage) {
            case 0: //mainscreen
                if(!noResults) {
                    var src = searchResults[actEl().tabIndex];
                    switch(src.type) {
                        case 'more':
                            ntext[1] = i18n('load');
                            break;
                        case 'video':
                        case 'playlistVideo':
                        case 'videoFromSubscription':
                            ntext[1] = i18n('watch');
                            break;
                        case 'userPlaylist':
                        case 'playlist': 
                            ntext[1] = i18n('view'); 
                            break;
                        case 'channel': 
                            ntext[1] = i18n('view'); 
                            break;
                        case 'playlistDetails': 
                            ntext[1] = i18n('details'); 
                            break;
                    }

                    if(submenuAllowed.indexOf(src.type) !== -1) {
                        ntext[2] = i18n('options');
                    }
                }
                break;
            case 1: //search bar
                ntext[1] = i18n('search');
                break;
            //case 2: //freetext screen
                //nothing special
                //break
            case 3: //user playlist modifier
                ntext[0] = null;
                ntext[1] = i18n('move');
                break;
            case 4: //submenu
                ntext[0] = i18n('back');
                ntext[1] = i18n('select');
                break;
            case 5: //user playlis rename
                ntext[0] = i18n('cancel');
                ntext[1] = i18n('save');
        }
        
    }

    outputNavbar(ntext);
}

function focusOnActiveSearchEntry() {
    document.getElementsByClassName('active')[0].focus();
}

function isUserPlaylist() {
    return type === 'userPlaylist';
}

function hideAuthor() {
    document.body.classList.add('hideauthor');
}

function nothingHere() {
    searchIconDisplay(
        i18n('nothing-here'),
        '/img/no-data.png'
    );
    noResults = true;
}

function removeActiveSearchResult(index) {
    if(index === undefined) {
        index = actEl().tabIndex;
    }

    searchResults.splice(index, 1);
    searchResultsElement.children[index].remove();
}

function srModifiedRefocus(targetFocus) {
    var focus = numclamp(
        0,
        searchResults.length - 1,
        targetFocus - 1
    );

    fixSearchResultsIndices(focus);
    resScroll(focus, true);
}

function fixSearchResultsIndices(start) {
    for(var i = start || 0; i < searchResults.length; i++) {
        searchResultsElement.children[i].tabIndex = i;
        searchResults[i].count = i;
    }
}