

(()=>{
    //scripts
    [
        'etc',
        'control',
        'subscriptions',
        'userPlaylistModifier',
        'submenu',
        'display',
        'search'
    ].forEach((fn)=>{
        addGlobalReference(0, fn);
    });

    //styles
    [
        'style',
        'carousel-style',
        'list-style',
        'infopanel'
    ].forEach((fn)=>{
        addGlobalReference(1, fn);
    });
})();