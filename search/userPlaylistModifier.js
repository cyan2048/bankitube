function playlistModifiedCommonAction() {
    clearUserPlaylistCache(query);
}

//========================================= user playlist moving things around
var userPlaylistModifierData = {
    classList: 'userPlaylistModifying'
};

function beginUserPlaylistModify() {
    userPlaylistModifierData.allowBack = allowBack;
    allowBack = false;
    curpage = 3;
    document.body.classList.add(userPlaylistModifierData.classList);
    userPlaylistModifyRefocus();
}

function commitUserPlaylistModify() {
    let ae = actEl();
    let target = 0;
    let start = ae.tabIndex;
	
    let done = ()=>{
        playlistModifiedCommonAction();
        disableControls = false;
        allowBack = userPlaylistModifierData.allowBack;
        delete userPlaylistModifierData.allowBack;
        curpage = 0;
        document.body.classList.remove(userPlaylistModifierData.classList);
        resScroll(ae.tabIndex, true);
        updatenavbar();
    };

    disableControls = true;
	
	//try and work out how much we moved
    if(ae.previousElementSibling) {
        target = ae.previousElementSibling.tabIndex;
		if(start - 1 > target) {target++} //add offset depending on if we moved the video further down the list
    } else {
		//we are the first element now
		//target = 0; //already set
	}
	
    //fix playlist list
    //the following is the only async thing so we will assign done() to this.
    userPlaylistModify(
        query,
        2,
        start,
        target,
        done,
        (e)=>{
            console.error(e);
            done();
        }
    );
    //fix search results
    arrayMove(
        searchResults,
        start,
        target
    );
    //fix the tabindcies
    fixSearchResultsIndices(Math.min(start, target));
}

function userPlaylistModifyMove(dir) {
    var srec = searchResultsElement,
    movingEl = actEl(),
    
    commonInsBf = (pfe)=>{ //its kinda confusing, but just imagine this block of code being in place of commonInsBf()s.
        if(pfe) {
            srec.insertBefore(
                movingEl,
                pfe
            );
        } else {
            srec.appendChild(movingEl);
        }
    };
    
    switch(dir) {
        case 1:
            var fe = movingEl.nextElementSibling;
            if(fe) {
                commonInsBf(fe.nextElementSibling);
            } else {
                srec.insertBefore(
                    movingEl,
                    srec.children[0]
                );
            }
            break;
        case -1:
            commonInsBf(movingEl.previousElementSibling);
            break;
    }

    movingEl.focus();

    userPlaylistModifyRefocus(dir);
}
function userPlaylistModifyRefocus(dir) {
    switch(displayMode) {
        case 0: //carousel
            var rvs = searchResultsElement, ae = actEl();
            rvs.scrollTo(
                ae.offsetLeft + (ae.offsetWidth / 2) - 
                (rvs.offsetWidth / 2)
            ,0);
            break;
        case 1: //list
            scrolliv(actEl(), dir > 0); 
            break;
    }
}

function userPlaylistModifyK(k) {
    if(keyisnav(k)) {k.preventDefault()}
    switch(displayMode) {
        case 0: //carosel
            switch(k.key) {
                case 'ArrowLeft':
                    var u = -1;
                case 'ArrowRight':
                    userPlaylistModifyMove(u || 1);
                    break;
                case 'Enter':
                case 'ArrowDown':
                    commitUserPlaylistModify();
                    break;
            }
            break;
        case 1: //list
            switch(k.key) {
                case 'ArrowUp':
                    var u = -1;
                case 'ArrowDown':
                    userPlaylistModifyMove(u || 1);
                    break;
                case 'Enter':
                case 'Backspace':
                    commitUserPlaylistModify();
                    break;
            }
    }
}

//========================================= user playlist simply just renaming it
var userPlaylistRenameMenu = new OptionsMenuSelectable(
    i18n('rename'),
    'text'
),
userPlaylistRenameData = {};
function userPlaylistRenameStart() {
    var c = searchResults[actEl().tabIndex];
    userPlaylistRenameData.lastFocus = actEl();
    userPlaylistRenameData.renaming = c.id;
    userPlaylistRenameMenu.setValue(c.name);
    userPlaylistRenameMenu.menuViewToggle(true, 2);
    curpage = 5;
}
function userPlaylistRenameEnd() {
    playlistModifiedCommonAction();
    userPlaylistRenameMenu.menuViewToggle(false);
    disableControls = false;
    userPlaylistRenameData.lastFocus.focus();
    curpage = 0;
    userPlaylistRenameData = {};
    updatenavbar();
}
function userPlaylistRenameK(k) {
    switch(k.key) {
        case 'Enter':
            var nn = userPlaylistRenameMenu.getValue().value;
            if(nn) {
                console.log(nn);
                var sucFn = ()=>{
                    var index = userPlaylistRenameData.lastFocus.tabIndex;

                    searchResults[index].name = nn;
                    switch(displayMode) {
                        case 0: //carousel
                            updateInfo();
                            break;
                        case 1: //list
                            userPlaylistRenameData
                                .lastFocus
                                .getElementsByClassName('search-results-title')[0]
                                .textContent = nn;
                            break;
                    }
                    userPlaylistRenameEnd();
                };
                actEl().blur();
                disableControls = true;
                userPlaylistManage(
                    userPlaylistRenameData.renaming,
                    2,
                    nn,
                    sucFn,
                    (e)=>{
                        //failed, show user.
                        console.error(e);
                        userPlaylistRenameEnd();
                    }
                );
            } else {
                alertMessage(
                    i18n('settings-invalid-input'),
                    5000,
                    0
                );
            }
            break;
        case 'Backspace':
        case 'SoftLeft':
            userPlaylistRenameEnd();
            break;
    }
}

//========================================= delete a video from playlist
function userPlaylistEntryDelete() {
    disableControls = true;
    var index = actEl().tabIndex,
    allDone = ()=>{
        playlistModifiedCommonAction();
        disableControls = false;
        updatenavbar();
    },
    doneFn = ()=>{
        alertMessage(i18n('playlist-video-remove-success'), 3000, 0);
        removeActiveSearchResult(index);
        if(searchResults.length === 0) {
            var delDone = ()=>{
                nothingHere();
                updateInfo(true);
                allDone();
            };
            userPlaylistManage(
                query,
                1,
                null,
                delDone,
                (e)=>{
                    console.log(e);
                    delDone();
                }
            )
        } else {
            srModifiedRefocus(index);
            updateInfo();
            allDone();
        }
    };

    userPlaylistModify(
        query,
        1,
        index,
        null,
        doneFn,
        (e)=>{
            console.log(e);
            doneFn();
        }
    );
}

//========================================= delete a playlist (the entire playlist)
function userPlaylistDelete(plid, plname) {
    var ii = actEl().tabIndex;

    messageBox(
        //i18n('are-you-sure'),
        plname,
        i18n('playlist-delete-confirmation'),
        {
            left: messageBoxOption(
                messageBoxDefaultBackCallback,
                i18n('no')
            ),
            right: messageBoxOption(
                ()=>{
                    messageBoxDefaultBackCallback();
                    disableControls = true;

                    var doneFn = ()=>{
                        removeActiveSearchResult(ii);
                        if(searchResults.length === 0) {
                            nothingHere();
                            updateInfo(true);
                        } else {
                            srModifiedRefocus(ii);
                            playlistModifiedCommonAction();
                            updateInfo();
                        }
                    
                        disableControls = false;
                        alertMessage(i18n('playlist-delete-success'), 3000, 0);
                        updatenavbar();
                    };
                
                    userPlaylistManage(
                        plid,
                        1,
                        null,
                        doneFn,
                        (e)=>{
                            console.log(e);
                            doneFn();
                        }
                    );
                },
                i18n('yes')
            )
        }
    );
}
