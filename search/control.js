var allowSearchBarFocus = true, curpage = 0;

function keyHandler(k) {
    switch(curpage) {
        case 0: mainScreenK(k); break;
        case 1: searchBarK(k); break;
        case 2: freeTextK(k); break;
        case 3: userPlaylistModifyK(k); break;
        case 4: submenuK(k); break;
        case 5: userPlaylistRenameK(k); break;
    }
}

function mainScreenK(k) {
    switch(k.key) {
        case 'Enter': itemAction(); break;
        case 'SoftLeft': navMainMenu(0); break;
        case 'SoftRight': openSubMenu(); break;
        default:
            switch(displayMode) { //how the thing is displayed (list, carosel, etc...)
                case 0: 
                    switch(k.key) {
                        case 'ArrowLeft':  k.preventDefault(); resScroll(-1); break;
                        case 'ArrowRight': k.preventDefault(); resScroll(1);  break;
                        case 'ArrowUp': 
                            if(type === 'userPlaylist') {
                                beginUserPlaylistModify();
                            } else {
                                focusSearchBar(true);
                            }
                            break;
                    }
                    break;
                case 1:
                    switch(k.key) {
                        case 'ArrowDown': k.preventDefault(); resScroll(1);  break;
                        case 'ArrowUp': 
                            k.preventDefault(); 
                            if(actEl().tabIndex === 0 && allowSearchBarFocus) {
                                focusSearchBar(true);
                            } else {
                                resScroll(-1); 
                            }
                            break;
                    }
                    break;
            }
            break;
    }
}

function searchBarK(k) {
    switch(k.key) {
        case 'Enter':
            var sq = actEl().value;
            if(sq.replace(/[\s\.]/g,'').length !== 0) {
                location.hash = 'q=' + encodeURIComponent(sq);
                location.reload();
            } else {
                alertMessage(i18n('enter-search-query'),1000,0);
            }
            break;
        case 'SoftLeft': navMainMenu(0); break;
        case 'ArrowDown':
            k.preventDefault();
        case 'Backspace':
            focusSearchBar(false);
            break;
    }
}

function focusSearchBar(tr) {
    if(allowSearchBarFocus) {
        var sra = document.getElementById('search-results-all'),
        sq = document.getElementById('search-query');
        if(tr) {
            sra.style.opacity = 0.25;
            focusInput(sq);
            curpage = 1;
        } else {
            if(!noResults) {
                sra.style.opacity = null;
                focusOnActiveSearchEntry();
                curpage = 0;
            }
        }
    }
}

function resScroll(dir,abs) {
    var rvs = searchResultsElement, rvsc = rvs.children;

    actEl().classList.remove('active');

    switch(displayMode) {
        case 0: //carousel
            if(abs) {
                nextel = rvsc[dir];
            } else {
                //dir should be either -1 or 1. we are not going to check tho
                nextel = actEl().tabIndex + dir;
                if(!(nextel in rvsc)) {
                    nextel += rvsc.length * (dir * -1);
                }
                nextel = rvsc[nextel];
            }
            nextel.focus();
            nextel.classList.add('active');
            rvs.scrollTo(
                nextel.offsetLeft + (nextel.offsetWidth / 2) - 
                (rvs.offsetWidth / 2)
            ,0);
            
            lazyLoadHandler(3,nextel,rvsc);
            updateInfo();
            break;
        case 1: //list
        var ne;
            if(abs) {
                ne = rvsc[dir];
            } else {
                /* 
                    navigatelist returns the new index.
                    select what navigatelist returns from rvsc, and add the class.
                */
               ne = rvsc[
                    navigatelist(
                        actEl().tabIndex,
                        rvsc,
                        dir
                    )
                ];
            }

            ne.focus(); //note - if abs is false focus() is called twice on the element.
            ne.classList.add('active');
            scrolliv(ne, dir > 0);
            lazyLoadHandler(3, ne, rvsc);
            break;
    }
}

function itemAction(subaction) {
    var csr = searchResults[actEl().tabIndex], prms = new URLSearchParams(),
    stp = [
        /* 0 */'video',
        /* 1 */'channel',
        /* 2 */'playlist',
        /* 3 */'more',
        /* 4 */'playlistVideo',
        /* 5 */'playlistDetails',
        /* 6 */'videoFromSubscription',
        /* 7 */'userPlaylist'
    ].indexOf(csr.type),
    subaction = subaction << 4,
    action = subaction + stp;
    /* 
    0000 0000
    ^    ^stp
    ^subaction

    accomidate up to 255 actions!

    stp:
    refer to that definition.

    subaction
    0: view
    1: listen
    the rest will be different depending on what stp is.
    */

    console.log(stp, subaction, action, csr);

    //note: returning true here will tell the submenu that curpage stuff will be dealt with here.

    switch(action) {
        case 0: //watch a video
        case 6: //watch a video (from subscriptions)
            location = `/watch/index.html#&v=${csr.id}`; 
            break;

        case 4: //watch a video in a playlist
            goToWatchScreenWithPlaylist(
                false,
                query,
                csr.id,
                csr.count,
                isUserPlaylist()
            ); 
            break;

        case 16: //listen to a video
        case 22: //listen to a video (from subscriptions)
            location = `/watch/index.html#listen=true&v=${csr.id}`; 
            break;

        case 20: //listen to something in playlist
            goToWatchScreenWithPlaylist(
                true,
                query,
                csr.id,
                csr.count,
                isUserPlaylist()
            ); 
            break;

        case 32: //save a video to a playlist (subaction: 2)
        case 38: //same as above but from subscriptions
        case 36: //same as above but from another playlist
            var cf = actEl();
            addToPlaylistSDAE(
                csr.id,
                ()=>{
                    mdataStoreSet(
                        csr.id,
                        {
                            name: csr.name,
                            type: 'video',
                            author: {
                                name: csr.author.name,
                                id: csr.author.id
                            },
                            time: csr.author.updated,
                            length: csr.length
                        }
                    );
                    cf.focus();
                    updatenavbar();
                },
                0
            );
            break;

        case 48: //share a video
        case 52: //same as above but from user playlist
        case 54: //same as bove but from subscirpnfsdl
            shareot(
                linkUtil.formYtVideoLink(csr.id),
                0
            );
            break;

        case 49: //share a channel
            shareot(
                linkUtil.formYtChannelLink(csr.author.id),
                0
            );
            break;

        case 50: //share a playlist
            shareot(
                linkUtil.formYtPlaylistLink(csr.id),
                0
            );
            break;

        case 1: //view a channel
            location = `/channel/index.html#${csr.author.id}`; 
            break;

        //case 16: //listen to a channel (???)
        case 3: //more action
            switch(typeof(continuation)) {
                case 'number': continuation++; break;
                //case 'string': prms.append('continuation',continuation); break;
                //else break
            }
            searchVideo(query,type);
            updatenavbar();
            break;

        case 2: //view a playlist
            prms.append('q',csr.id);
            prms.append('type','playlist');
            prms.append('headertext',csr.name);
            location.hash = prms.toString();
            break;

        case 5: //view playlist descriptions
            showFreeTextScreen(
                csr.description.plain
            );
            break;

        case 53: //share playlist (from playlist description)
            shareot(
                linkUtil.formYtPlaylistLink(query),
                0
            );
            break;

        case 18: //listen to a playlist
            if(csr.extra.video) {
                pushToAllHistory(
                    csr.id,
                    'playlist',
                    csr.name,
                    {
                        video: csr.extra.video
                    }
                );
                goToWatchScreenWithPlaylist(
                    true,
                    csr.id,
                    csr.extra.video,
                    0
                );
            }
            break;

            case 7: //view userplaylist
                prms.append('q',csr.id);
                prms.append('type','userPlaylist');
                location.hash = prms.toString();
                break;

            case 68: //move a video in a userplaylist
                beginUserPlaylistModify();
                break;
            
            case 87: //rename a userplaylist
                userPlaylistRenameStart();
                break;

            case 100: //remove a video in userplaylist
                userPlaylistEntryDelete();
                break;

            case 103: //remove a user playlist (the entire playlist)
                userPlaylistDelete(csr.id, csr.name);
                break;
    }
}