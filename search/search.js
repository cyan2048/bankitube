disableControls = false;
var loaded = false, //loaded is "first loaded"
continuation = null, 
query, queryTitle, type = 'video', fields, searchResults = [], noResults = false;

window.addEventListener('DOMContentLoaded',() => {
    infoPanelShow(-1);
    updateView();

    var lchash = decodeURIComponent(location.hash.substr(1));
    if(lchash) {
        lchash = new URLSearchParams(lchash);
        query = lchash.get('q');
        type = lchash.get('type');
        fields = lchash.get('fields');

        var setHeader = (text, icon)=>{
            eid('nosearch-text').textContent = queryTitle = text;
            eid('nosearch-img').src = '/img/' + icon + '.png';
        };

        if([
            'channel',
            'playlist',
            'userPlaylist',
            'userPlaylistList',
            'channelPlaylists',
            'subscriptions'
        ].indexOf(type) === -1) {
            eid('search-query').value = query;
        } else {
            document.body.classList.add('nosearch');

            switch(type) {
                case 'channel':
                    continuation = 1;
                case 'channelPlaylists':
                    hideAuthor();
                    break;
                case 'subscriptions':
                    pushHistory('subscriptionspage');
                    showLoadingScreen();
                    setHeader(
                        i18n('feed'),
                        'subscriptions'
                    );
                    getLatestFromSubscriptions(
                        (r)=>{
                            searchVideoDone(r.response)
                        },
                        (c,t) => {
                            eid('loading-overlay-text').innerHTML = `${i18n('loading')}<br>(${c}/${t})`;
                        },
                        5
                    );
                    return;
                case 'userPlaylistList':
                    hideAuthor();

                    var usp = userPlaylistMdataCache(), uspk = Object.keys(usp), uspa = [];

                    allowSearchBarFocus = false;

                    pushHistory(null,'userplaylistlist',null);

                    setHeader(
                        i18n('playlists-menu-item'),
                        'playlist'
                    );

                    uspk.forEach((k)=>{
                        var d = usp[k], entry = {};

                        /* count, image, name */

                        entry.type = 'userPlaylist';
                        entry.playlistId = k;
                        entry.title = d.name;
                        entry.videoCount = d.count;
                        entry.playlistThumbnail = d.image;

                        uspa.push(entry);
                    });

                    uspk = undefined;
                    usp = undefined;

                    uspa.sort((a,b)=>{
                        return a.title > b.title;
                    });

                    searchVideoDone(uspa);
                    return;
                case 'userPlaylist':
                    var playlistMdata = userPlaylistMdataCache();
                    if(query in playlistMdata) {
                        playlistMdata = playlistMdata[query];
                    }

                    allowSearchBarFocus = false;

                    pushHistory(query,'userplaylist',playlistMdata.name);

                    setHeader(
                        playlistMdata.name,
                        'playlist'
                    );

                    getUserPlaylistASR(
                        query, 
                        searchVideoDone
                    );
                    return;
            }
        }

        setHeader(lchash.get('headertext'), type);

        searchVideo(query,type,fields);
    } else {
        noResults = true;
        eid('search-query').value = '';
        focusSearchBar(true);
        updatenavbar();
        pushHistory('dummy');
    }
});

function searchVideo(q,type,fields) {
    var prms = new URLSearchParams(), endpoint = 'search'/* , cachedData */;

    switch(typeof(continuation)) {
        case 'number': prms.append('page',continuation); break;
        case 'string': prms.append('continuation',continuation); break;
        //else break
    }
    if(fields) {prms.append('fields',fields);}
    switch(type) {
        case 'channel':
            endpoint = 'channels/videos/' + q;
            pushHistory(q,'channelvideos',queryTitle);
            allowSearchBarFocus = false;
            break;
        case 'channelPlaylists':
            endpoint = 'channels/playlists/' + q;
            pushHistory(q,'channelplaylists',queryTitle);
            allowSearchBarFocus = false;
            break;
        case 'playlist':
            endpoint = 'playlists/' + q;
            pushHistory(q,'playlist',queryTitle);
            pushToAllHistory(q,'playlist');
            allowSearchBarFocus = false;
            break;
        default:
            prms.append('type', 'all');
            pushHistory(q,'search',q);
            pushToAllHistory(q,'search',q);
            prms.append('q', q);
            break;
    }

    requestItem(
        endpoint,
        '',
        prms.toString(),
        searchVideoDone,
        searchVideoError,
        loaded,
        true
    );


    showLoadingScreen();
}

function showLoadingScreen() {
    disableControls = true;
    eid('loading-overlay').classList.remove('hidden');
    eid('loading-overlay-b').classList.remove('hidden');
    eid('loading-overlay-s').classList.add('hidden');
    eid('loading-overlay-text').textContent = i18n('loading');
}

function searchIconDisplay(text,icon) {
    eid('loading-overlay').classList.remove('hidden');
    eid('loading-overlay-s').classList.remove('hidden');
    eid('loading-overlay-s').src = icon || '/img/search-big.png';
    eid('loading-overlay-b').classList.add('hidden');
    eid('loading-overlay-text').textContent = text;
}

function searchVideoError() {
    alertMessage(i18n('search-results-failed'),5000,2);
    if(searchResultsElement.children.length === 0) {
        searchIconDisplay(i18n('search-results-failed-short'));
        noResults = true;
        focusSearchBar(true);
        updatenavbar();
    } else {
        resScroll(
            searchResultsElement.children.length - 1,
            true
        );
        searchVideoFinalize();
    }
}

function searchVideoDone(rqResp) {
    //console.log(rqResp);
    disableControls = false;
    eid('loading-overlay').classList.add('hidden');
    eid('loading-overlay-box').classList.add('loaded');

    var resList, resListLen = 0, start = 0, noMore = false, tempSr = [];
    switch(type) {
        default:
			if('videos' in rqResp) {
				resList = rqResp.videos;
				if('continuation' in rqResp) {
					continuation = rqResp.continuation;
				}
			} else {
				resList = rqResp;
			}
			break;
        case 'channelPlaylists':
            resList = rqResp.playlists;
            for(var i = 0; i < resList.length; i++) {
                resList[i].type = 'playlist';
            }
            continuation = rqResp.continuation;
            break;
        case 'playlist':
            resList = rqResp.videos;
            if(0 in resList) { //the "else" of this "if" is lower down, where we check resList.length === 0

                mdataStoreSet(
                    query,
                    {
                        name: rqResp.title,
                        type: 'playlist'
                    }
                );

                eid('nosearch-text').textContent = rqResp.title;
            }
            if(resList.length < 100) { //if less than 100 then that means there is no more "next page"
                noMore = true;
            }
            for(var i = 0; i < resList.length; i++) {
                resList[i].type = 'playlistVideo';
            }
    
            if(!loaded) {
                resList.unshift({
                    type: 'playlistDetails',
                    name: rqResp.title,
                    description: {
                        plain: rqResp.description,
                        html: rqResp.descriptionHtml
                    },
                    updated: unixdateformat(rqResp.updated),
                    count: commaSeparateNumber(rqResp.videoCount),
                    length: null,
                    author: {
                        name: rqResp.author,
                        id: rqResp.authorId
                    },
                    id: null,
                    images: ['/img/thumbnail-playlist.png']
                });
            }
            break;
    }
    resListLen = resList.length;
    if(resListLen === 0) {noMore = true}
    if([
        'userPlaylist',
        'userPlaylistList'
    ].indexOf(type) === -1) {resList.push({type: 'more'});}
    if(loaded) {
        start = searchResultsElement.children.length - 1;
        searchResultsElement.children[start].remove();
        searchResults.splice(start,1);
    } else {
        loaded = true;
        if(resListLen === 0) {
            nothingHere();

            noResults = true;
            focusSearchBar(true);
            updatenavbar();
            return;
        }
    }

    var noDescText = i18n('video-info-no-description');

    var resultsPrinted = 0;
    resList.forEach((e) => {
        var res = {
            type: e.type,
            name: null,
            description: null,
            updated: null,
            count: 0,
            length: null,
            /* live: false,
            upcoming: false, */
            author: {
                name: null,
                id: null,
                subcount: 0
            },
            id: null,
            images: [],
            extra: {}
        }

        switch(e.type) {
            case 'videoFromSubscription': 
                res = e; 
                noMore = true;
                break;
            case 'video':
                if(
                    searchResults.filter((f) => {
                        return f.id === e.videoId
                    }).length !== 0
                ) {noMore = true;return;}

                res.name = e.title;
                if(vidIsDed(e)) {
                    res.type = 'dead';
                } else {
                    res.id = e.videoId;
                    res.author.name = e.author;
                    res.author.id = e.authorId;
                    res.updated = e.published;
                    res.description = e.description || noDescText;
                    res.live = !!e.liveNow;
                    res.upcoming = !!e.isUpcoming;
                    res.length = e.lengthSeconds;
                    res.count = commaSeparateNumber(e.viewCount);
                    res.images.push(thumbchoose(e.videoThumbnails));
                }
                break;
            case 'playlistVideo':
                if(
                    searchResults.filter((f) => {
                        return f.id === e.videoId
                    }).length !== 0
                ) {noMore = true;return;}
                
                res.name = e.title;
                if(vidIsDed(e)) {
                    res.type = 'dead';
                } else {
                    res.id = e.videoId;
                    res.author.name = e.author;
                    res.author.id = e.authorId;
                    res.count = e.index;
                    res.length = e.lengthSeconds;
                    res.images.push(thumbchoose(e.videoThumbnails));
                }
                break;
            case 'playlist':
                if(
                    searchResults.filter((f) => {
                        return f.id === e.playlistId
                    }).length !== 0
                ) {noMore = true;return;}
                res.author.name = e.author;
                res.extra.video = (0 in e.videos ? e.videos[0].videoId : null);
                //continue on, userPlaylist will just start frome here.
            case 'userPlaylist':
                res.id = e.playlistId;
                res.name = e.title;
                res.count = commaSeparateNumber(e.videoCount);

                if('playlistThumbnail' in e) {
                    var t;
                    if(res.type === 'playlist') {
                        t = e.playlistThumbnail;
                    } else {
                        t = linkUtil.formYtThumbnail(e.playlistThumbnail);
                    }
                    res.images.push(t);
                } else {
                    e.videos.forEach((ee) => {
                        res.images.push(thumbchoose(ee.videoThumbnails));
                    });
                }
                break;
            case 'playlistDetails':
                res = e;
                break;
            case 'channel':
                if(
                    searchResults.filter((f) => {
                        return f.author.id === e.authorId
                    }).length !== 0
                ) {noMore = true;return;}
                res.author.id = e.authorId;
                res.author.name = e.author;
                res.description = e.description || noDescText;
                res.author.subcount = e.subCount;
                res.count = e.videoCount;
                var chimg = imgChoose(e.authorThumbnails,'width',200);
                if(chimg) {
                    chimg = 'https:' + chimg;
                } else {
                    chimg = '/img/nouserimage-colored.png';
                }
                res.images.push(chimg);
                break;
            case 'more':
                if(noMore) {return;}
                res.images.push('/img/more.png'); 
                res.name = i18n('load-more-ellipses');
                break;
            case 'category':
                return; //we are in a foreach(), use return to skip something.
        }

        outputElements[cdisplayModeName()].individual(res, resultsPrinted, start);

        resultsPrinted++;

        tempSr.push(res);
    });


    tempSr.forEach((e) => {searchResults.push(e);});
    if(resultsPrinted === 0) {
        start -= 1;
        alertMessage(i18n('search-results-no-more'),3000,0);
    }
    resScroll(start,true);
    searchVideoFinalize();
}

function searchVideoFinalize() {
    updateInfo();
    updatenavbar();
}


