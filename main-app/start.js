var appIframe = null;
function start() {
    if(
		!sessionStorage.getItem('apprunning') &&
		pageIsVisibleFromStartup()
	) {
		
		if(startupWhileCheckingCheck !== null) {
			clearInterval(startupWhileCheckingCheck);
		}
		
        var sif = document.createElement('iframe');
        sif.src = '/settings/index.html#bootup';
        sif.width = window.innerWidth;
        sif.height = window.innerHeight;
        sif.allowFullscreen = true;
        sif.frameBorder = 0;
        document.body.appendChild(sif);

        //due to lazyness i will just set interval this to make sure it is focused
        var focusInt = setInterval(
            ()=>{
                sif.focus();
                console.log('focus!!!')
            },
            250
        );
        setTimeout(
            ()=>{clearInterval(focusInt);},
            2000
        );
        sif.focus();

        appIframe = sif;

        eid('loading-indicator').remove();

        start = undefined;
    }
}

window.addEventListener('DOMContentLoaded',()=>{
    var selLang = getSettingValue('language');
    if(selLang === null) {
        localizationUpdateObjects();
        localizationInit();
        start();
    } else {
		freedatagrab(
			'/common/lang/list',
			(rawlist)=>{
				let list = rawlist.split(/\r?\n/g);
				for(let i = 0; i < list.length; ) {
					let line = list[i];
					if(line.substring(0, 2) !== '//') {
						line = line.split('\t');
						if(line.length === 2) {
							list[i] = {
								fileName: line[0],
								languageName: line[1]
							};
							i++;
							continue;
						}
					}
					list.shift();
					//continue;
				}
				
				var lpe = ['/common/lang/', '.json'];
				freedatagrab(
					lpe[0] + list[getSettingValue('language')].fileName + lpe[1],
					(d)=>{
						//console.log(d);
						
						localizationUpdateObjects(d);
						localizationInit();
			
						startupWhileCheckingCheck = setInterval(()=>{
							start();
						}, 500);
						start();
					},
					()=>{alert('fatal error')},
					false,
					true
				);
			},
			()=>{alert('fatal error')},
			false,
			false
		);
    }
    
    mdataStoreRmnr();

    window.addEventListener('keydown',(k)=>{
        if(keyisnav(k)) {
            k.preventDefault();
        }

        if(appIframe) {
            appIframe.focus();
        }
    });
});