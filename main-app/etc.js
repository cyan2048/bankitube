var alertMessageTO = false,
alertMessageEl = eid('alertMessage');
function alertMessage(message,disptime,type) {
    if(alertMessageTO !== false) {
        clearTimeout(alertMessageTO);
    }
    switch(type) {
        default:
            alertMessageEl.style.background = null;
            alertMessageEl.style.color = null;
            break;
        case 0:
            alertMessageEl.style.background = '#2a2a2a';
            alertMessageEl.style.color = '#fff';
            break;
        case 1:
            alertMessageEl.style.background = '#fff';
            alertMessageEl.style.color = '#000';
            break;
        case 2:
            alertMessageEl.style.background = '#ff0';
            alertMessageEl.style.color = '#000';
            break;
        case 3:
            alertMessageEl.style.background = '#f00';
            alertMessageEl.style.color = null;
            break;
    }
    alertMessageEl.textContent = message;
    if(alertMessageEl.classList.contains('showing')) {
        alertMessageEl.classList.remove('nudgeAnimation');
        void alertMessageEl.offsetHeight;
        alertMessageEl.classList.add('nudgeAnimation');
    } else {
        alertMessageEl.classList.add('showing');
    }
    
    if(disptime > 0) {
        alertMessageTO = setTimeout(function(){
            alertMessageTO = false;
            alertMessageEl.classList.remove('showing');
        },disptime);
    }
}

function fullReload() {
    sessionStorage.removeItem('apprunning');
    location.reload();
}