if('mozSetMessageHandler' in navigator) {
	navigator.mozSetMessageHandler('activity', function(activityRequest) {
		console.log(activityRequest);

		var match = activityRequest.source.data.url.match(youtubeRegex),
		op = {
			id: match[2],
			target: null
		};
		switch(match[1]) {
			case 'youtube.com/channel/':
				op.target = 'channel';
				break;
			case 'youtube.com/watch?v=':
			case 'youtube.com/embed/':
			case 'youtube.com/shorts/':
			case 'youtu.be/':
				op.target = 'watch';
				break;
			case 'youtube.com/playlist?list=':
				op.target = 'playlist';
				break;
		}

		sessionStorage.setItem(
			'open',
			JSON.stringify(op)
		);

		if(sessionStorage.getItem('apprunning')) {
			sessionStorage.removeItem('apprunning');
			fullReload();
		}
	});
}