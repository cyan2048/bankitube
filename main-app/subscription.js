var channelSubscriptions = localforage.createInstance({
    name: 'BankiTube',
    storeName: 'subscriptions',
});

function subscriptionsCheck(id,sfn,efn) {
    channelSubscriptions.getItem(id).then(sfn || emptyFn).catch(efn || emptyFn);
}

function subscriptionsAdd(id,name,img,dontNotify,doneFn) {
    subscriptionsCheck(
        id, (r)=>{
            if(r === null) {
                channelSubscriptions.setItem(id, (new Date()).getTime() / 1000).then(()=>{
                    if(!dontNotify) {
                        alertMessage(
                            i18n('subscription-added'),
                            3000,0
                        );
                    }
                });

                subscriptionsUpdateMdata(id, name, img, doneFn);
            } else {
                console.warn(`subscriptionsAdd: ${id} is already subscribed; nothing changed.`);
                doneFn();
            }
        }
    );
}

function subscriptionsUpdateMdata(id,name,img,doneFn) {
    //return;
    mdataStoreSet(id, {
        name: name,
        image: img,
        type: 'channel'
    }, doneFn);
}

function subscriptionsRemove(id,dontNotify,doneFn) {
    channelSubscriptions.removeItem(id).then(()=>{
        if(!dontNotify) {
            alertMessage(i18n('subscription-removed'),3000,0);
        }
        doneFn();
    });
}
