function keyHandler(k) {
    if(keyisnav(k)) {k.preventDefault()}
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown': 
            navHistory(u || 1);
            break;
        case 'ArrowLeft': focusCategory(-1); break;
        case 'ArrowRight': focusCategory(1); break;
        case 'Enter': openHistoryEntry(); break;
        case 'SoftRight': 
            if(actEl().dataset.type === 'video') {
                openHistoryEntry({'listen':true});
            }
            break;
        case 'SoftLeft': navMainMenu(0); break;
    }
}

function localupdatenavbar() {
    outputNavbar(i18n('menu'));
    var ce = {
        'video': i18n('watch'),
        'channel': i18n('view'),
        'playlist': i18n('view'),
        'search': i18n('search')
    },
    le = {
        'video': i18n('listen')
    };
    if('id' in actEl().dataset) {
        var ct = actEl().dataset.type;
        outputNavbar(
            i18n('menu'),
            ce[ct] || '',
            le[ct] || ''
        );
    } else {
        outputNavbar(i18n('menu'));
    }
}